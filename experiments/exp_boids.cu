#include <iostream>
#include <string>
#include <mcleap_mesh.cuh>
#include <MCleap.h>
#include <frnn_kernels.cuh>

#include <random>

#include "helper_functions.cuh"
#include "helper_functions.h"

#include "stats.cuh"

#define WIDTH 900
#define HEIGHT 900

#define A_FORCE 100.0
#define B_FORCE 5.0
#define C_FORCE 40.0

#define RADA 14.0
#define RADB 20.0
#define RADC 40.0

/*
#define INITIAL_SIM_MESH "../resources/mesh100000.off"
#define SIM_SCALING 64.0
#define MAX_VEL 30.0
#define RAD_SCALING 1.0 // scales the radiuses using this parameter

#define TIMESTEP 1.0 / 480.0
*/

using MCleap::MCLEAP_VEC;
using MCleap::MCLEAP_REAL;

struct calcForces_functor {
	calcForces_functor(int numVertices, MCLEAP_VEC* vel, MCLEAP_VEC* separation, MCLEAP_VEC* cohesion, MCLEAP_VEC* alignement, int* num_A, int* num_B, int* num_C, float rad_scale) :
		numVertices(numVertices), vel(vel), separation(separation), cohesion(cohesion), alignement(alignement), num_A(num_A), num_B(num_B), num_C(num_C) {}
	void resetForces() {
		cudaMemset(separation, 0.0, numVertices * sizeof(MCLEAP_REAL) * 2);
		cudaMemset(cohesion, 0.0, numVertices * sizeof(MCLEAP_REAL) * 2);
		cudaMemset(alignement, 0.0, numVertices * sizeof(MCLEAP_REAL) * 2);
		cudaMemset(num_A, 0, numVertices * sizeof(int));
		cudaMemset(num_B, 0, numVertices * sizeof(int));
		cudaMemset(num_C, 0, numVertices * sizeof(int));
		//cudaMemset(counter, 0, 1 * sizeof(int));
		CudaCheckError();
	}
	__device__ void operator()(const int& i, const int& j, MCLEAP_VEC& dist_vec, double& dist) {
		//if (i == 1234)atomicAdd(counter,1);
		if (dist <= RADA*rad_scale) {
			separation[i].x += dist_vec.x;
			separation[i].y += dist_vec.y;
			num_A[i] += 1;
		}
		if (dist <= RADB*rad_scale) {
			cohesion[i].x += dist_vec.x;
			cohesion[i].y += dist_vec.y;
			num_B[i] += 1;
		}
		if (dist <= RADC*rad_scale) {
			MCLEAP_VEC norm_vel = vel[j] / (MCLEAP_REAL)mcleap_d_magnitude(vel[j]);
			alignement[i].x += norm_vel.x;
			alignement[i].y += norm_vel.y;
			num_C[i] += 1;
		}
	}
	int numVertices;
	MCLEAP_VEC* vel;
	MCLEAP_VEC* separation, * cohesion, * alignement;
	int* num_A, * num_B, * num_C;
	float rad_scale;
};

__device__ MCLEAP_REAL clamp(MCLEAP_REAL a, MCLEAP_REAL b, MCLEAP_REAL c) {
	if (a < b)return b;
	if (a > c)return c;
	return a;
}

__device__ MCLEAP_REAL kernel_walls(MCLEAP_REAL x) {
	return -5 * exp(-0.01 * x * x + 4);
}

__global__ void move_mesh_kernel(int numVertices, MCLEAP_VEC* vertexData, MCLEAP_VEC* velocity, const float dt, MCLEAP_VEC* separation, MCLEAP_VEC* cohesion, MCLEAP_VEC* alignement, int* num_A, int* num_B, int* num_C, MCLEAP_VEC* displacement, float max_vel, float sim_scaling) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < numVertices) {
		if (num_A[i] > 0)separation[i].x = (separation[i].x * A_FORCE) / (MCLEAP_REAL)num_A[i];
		if (num_A[i] > 0)separation[i].y = (separation[i].y * A_FORCE) / (MCLEAP_REAL)num_A[i];

		if (num_B[i] > 0)cohesion[i].x = (cohesion[i].x * B_FORCE) / (MCLEAP_REAL)num_B[i];
		if (num_B[i] > 0)cohesion[i].y = (cohesion[i].y * B_FORCE) / (MCLEAP_REAL)num_B[i];

		if (num_C[i] > 0)alignement[i].x = (alignement[i].x * C_FORCE) / (MCLEAP_REAL)num_C[i];
		if (num_C[i] > 0)alignement[i].y = (alignement[i].y * C_FORCE) / (MCLEAP_REAL)num_C[i];

		MCLEAP_VEC acel = MCLEAP_VEC{ separation[i].x + cohesion[i].x + alignement[i].x, separation[i].y + cohesion[i].y + alignement[i].y };

		MCLEAP_REAL box_size = sim_scaling * 100.0;

		acel.x -= kernel_walls(vertexData[i].x + box_size);
		acel.x += kernel_walls(vertexData[i].x - box_size);
		acel.y -= kernel_walls(vertexData[i].y + box_size);
		acel.y += kernel_walls(vertexData[i].y - box_size);

		velocity[i].x += acel.x * dt;
		velocity[i].y += acel.y * dt;

		velocity[i].x = clamp(velocity[i].x, -max_vel, max_vel);
		velocity[i].y = clamp(velocity[i].y, -max_vel, max_vel);

		displacement[i].x = velocity[i].x * dt;
		displacement[i].y = velocity[i].y * dt;
	}
}


__global__ void avoid_interpenetration_kernel(int numVertices, MCLEAP_VEC* vertexData, MCLEAP_VEC* velocity, int numEdges, MCleap::edge* hed) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < numEdges && (hed[i].tb.x != -1)) {
		int id0 = hed[i].v.x;
		int id1 = hed[i].v.y;
		MCLEAP_VEC v0 = vertexData[id0] + MCLEAP_VEC{ velocity[id0].x,velocity[id0].y };
		MCLEAP_VEC v1 = vertexData[id1] + MCLEAP_VEC{ velocity[id1].x,velocity[id1].y };
	}
}

__global__ void scale_mesh_kernel(int numVertices, MCLEAP_VEC* vertexData, float scale) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < numVertices) {
		vertexData[i].x *= scale;
		vertexData[i].y *= scale;
	}
}

void avoid_interpenetration(MCleap::mcleap_mesh* m, MCLEAP_VEC* velocity) {
	MCleap::map_cuda_buffers(m);

	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_edges + block_size - 1) / dimBlock.x);

	avoid_interpenetration_kernel << <dimGrid, dimBlock >> > (m->num_vertices, m->d_vbo_v, velocity, m->num_edges, m->d_mesh->he);
}

void move_mesh(MCleap::mcleap_mesh* m, MCLEAP_VEC* velocity, const float dt, MCLEAP_VEC* separation, MCLEAP_VEC* cohesion, MCLEAP_VEC* alignement, int* num_A, int* num_B, int* num_C, MCLEAP_VEC* displacement, float max_vel, float sim_scaling) {
	MCleap::map_cuda_buffers(m);

	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_vertices + block_size - 1) / dimBlock.x);

	move_mesh_kernel << <dimGrid, dimBlock >> > (m->num_vertices, m->d_vbo_v, velocity, dt, separation, cohesion, alignement, num_A, num_B, num_C, displacement, max_vel, sim_scaling);
}

void scale_mesh(MCleap::mcleap_mesh* m, float scale) {
	MCleap::map_cuda_buffers(m);

	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_vertices + block_size - 1) / dimBlock.x);
	CudaCheckError();
	scale_mesh_kernel << <dimGrid, dimBlock >> > (m->num_vertices, m->d_vbo_v, scale);
	CudaCheckError();
}

template<typename Functor, int queueSize, int visitedSizePerVertex>
void calcForces(Functor f, MCleap::mcleap_mesh* m, MCLEAP_VEC* d_vel, int* d_visited, float rad_scale) {
	MCleap::map_cuda_buffers(m);

	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_vertices + block_size - 1) / dimBlock.x);

	cudaMemset(d_visited, 0, sizeof(int) * m->num_vertices * visitedSizePerVertex);
	cudaDeviceSynchronize();
	calcFRNN_dfs<Functor, queueSize, visitedSizePerVertex> << <dimGrid, dimBlock >> > (f, m->d_vbo_v, m->d_t, m->d_mesh->he, m->d_mesh->v_to_he, d_visited, m->num_vertices, RADC*rad_scale);
	cudaDeviceSynchronize();
	CudaCheckError();
}

int main(int argc, const char* argv[]) {

	std::string meshes[] = { std::string("../resources/mesh100000.off"), std::string("../resources/mesh_grid_1000000.off") };
	//float max_vels[] = { 2.0, 10.0, 20.0, 30.0, 40.0, 50.0 };
	float max_vel = 30.0;
	float sim_scaling[] = { 64.0, 128.0 };
	float rad_scaling[] = { 10.0 };
	float time_steps[] = { 0.001, 0.0001 };

	int num_realizations = 3;

	FILE* fp;
	char name[50];
	int roll_no, i, n;
	float marks;

	fp = fopen("delaunay_dfs.txt", "w");

	if (fp == NULL) {
		printf("file can't be opened\n");
		exit(1);
	}
	double progress = 0.0;
	for (auto mesh : meshes)for (auto sim_scale : sim_scaling)for (auto rad_scale : rad_scaling)for (auto timestep : time_steps)
	{
		printProgress(progress);
		progress += 1.0/8.0; // 1/8 cases
		fprintf(fp,"mesh, sim_scaling, rad_scaling, time_step\n");
		fprintf(fp,"%s, %f, %f, %f\n", mesh.c_str(), sim_scale, rad_scale, timestep);

		fprintf(fp,"Realizations\n%d\n", num_realizations);

		statistics stats_update_outer;
		RunningStat meas_update_outer;

		statistics stats_calc_outer;
		RunningStat meas_calc_outer;

		statistics stats_total_outer;
		RunningStat meas_total_outer;

		for (int REALIZATION = 0; REALIZATION < num_realizations; REALIZATION++)
		{

			statistics stats_update;
			RunningStat meas_update;

			statistics stats_calc;
			RunningStat meas_calc;

			statistics stats_total;
			RunningStat meas_total;

			MCleap::mcleap_mesh* m;
			{
				auto begin = std::chrono::high_resolution_clock::now();

				m = MCleap::load_off_mesh(mesh);

				auto end = std::chrono::high_resolution_clock::now();
				auto diff = std::chrono::duration<float, std::milli>(end - begin).count();
				fprintf(fp, "build_delaunay %f\n", diff);
			}
			CudaCheckError();

			if (m == nullptr) {
				printf("Coudn't find the file \" %s \" \n", mesh.c_str());
				continue;
			}

			scale_mesh(m, sim_scale);
			CudaCheckError();

			MCLEAP_VEC* h_vel = new MCLEAP_VEC[m->num_vertices], * d_vel;
			h_vel[0].x = 0.0;
			h_vel[0].y = 0.0;
			h_vel[1].x = 0.0;
			h_vel[1].y = 0.0;
			h_vel[2].x = 0.0;
			h_vel[2].y = 0.0;
			h_vel[3].x = 0.0;
			h_vel[3].y = 0.0;
			for (int i = 4; i < m->num_vertices; i++) {
				h_vel[i].x = (i % 2 ? 1 : -1) * ((MCLEAP_REAL)i / m->num_vertices) * max_vel;
				h_vel[i].y = (i % 2 ? -1 : 1) * ((MCLEAP_REAL)i / m->num_vertices) * max_vel;
			}
			cudaMalloc((void**)&d_vel, m->num_vertices * sizeof(MCLEAP_VEC));
			cudaMemcpy(d_vel, h_vel, m->num_vertices * sizeof(MCLEAP_VEC), cudaMemcpyHostToDevice);
			CudaCheckError();

			{
				auto begin = std::chrono::high_resolution_clock::now();

				if(MCleap::delaunay_transformation(m)!=1)continue;

				auto end = std::chrono::high_resolution_clock::now();
				auto diff = std::chrono::duration<float, std::milli>(end - begin).count();
				fprintf(fp, "delonize_mesh %f\n", diff);

			}
			CudaCheckError();

			int* d_visited;
			constexpr int queueSize = 200;
			constexpr int visitedPerVertex = 200;

			cudaMalloc((void**)&d_visited, sizeof(int) * m->num_vertices * visitedPerVertex);

			int numVertices = m->num_vertices;
			MCLEAP_VEC* separation, * cohesion, * alignement;
			int* num_A, * num_B, * num_C;
			MCLEAP_VEC* d_displacement;
			int* d_counter;
			cudaMalloc((void**)&separation, numVertices * sizeof(MCLEAP_VEC));
			cudaMalloc((void**)&cohesion, numVertices * sizeof(MCLEAP_VEC));
			cudaMalloc((void**)&alignement, numVertices * sizeof(MCLEAP_VEC));
			cudaMalloc((void**)&num_A, numVertices * sizeof(int));
			cudaMalloc((void**)&num_B, numVertices * sizeof(int));
			cudaMalloc((void**)&num_C, numVertices * sizeof(int));
			cudaMalloc((void**)&d_displacement, numVertices * sizeof(MCLEAP_VEC));
			cudaMalloc((void**)&d_counter, sizeof(int));

			calcForces_functor f(m->num_vertices, d_vel, separation, cohesion, alignement, num_A, num_B, num_C, rad_scale);
			//f.counter = d_counter;
			//CudaCheckError();

			MCleap::mcleap_mesh* m_copy = MCleap::init_empty_mesh(m->num_vertices, m->num_edges, m->num_triangles);

			//int itercount = 60 * (1.0 / timestep); // in order to make 1 minute of simulation
			int itercount = 100;
			fprintf(fp, "Number of iterations (trace):\n%d\n", itercount);

			for (int i = 0; i < itercount; i++) {
				float diff1, diff2, diff3;
				auto begin1 = std::chrono::high_resolution_clock::now();
				// FRNN
				{
				auto begin = std::chrono::high_resolution_clock::now();
				f.resetForces();
				calcForces<calcForces_functor, queueSize, visitedPerVertex>(f, m, d_vel, d_visited, rad_scale);
				//avoid_interpenetration(m, d_vel);
				//MCleap::mcleap_mesh* m, MCLEAP_VEC* velocity, const float dt, MCLEAP_VEC* separation, MCLEAP_VEC* cohesion, MCLEAP_VEC* alignement, int* num_A, int* num_B, int* num_C, MCLEAP_VEC* displacement, float max_vel, float sim_scaling
				move_mesh(m, d_vel, timestep, separation, cohesion, alignement, num_A, num_B, num_C, d_displacement, max_vel, sim_scale);
				auto end = std::chrono::high_resolution_clock::now();
				auto diff = std::chrono::duration<float, std::milli>(end - begin).count();
				diff1 = diff;
				meas_calc.Push(diff);
				}
				// UPDATE
				{
					auto begin = std::chrono::high_resolution_clock::now();
					MCleap::move_vertices(m, m_copy, (MCLEAP_REAL*)d_displacement);
					MCleap::unmap_cuda_buffers(m); 
					auto end = std::chrono::high_resolution_clock::now();
					auto diff = std::chrono::duration<float, std::milli>(end - begin).count();
					diff2 = diff;
					meas_update.Push(diff);
				}
				auto end = std::chrono::high_resolution_clock::now();
				auto diff = std::chrono::duration<float, std::milli>(end - begin1).count();
				diff3 = diff;
				fprintf(fp,"%f %f %f\n", diff1, diff2, diff3);
				meas_total.Push(diff);
			}

			stats_calc.mean = meas_calc.Mean();
			stats_calc.variance = meas_calc.Variance();
			stats_calc.stdev = meas_calc.StandardDeviation();
			stats_calc.sterr = meas_calc.StandardDeviation() / ((double)sqrt(meas_calc.NumDataValues()));

			stats_update.mean = meas_update.Mean();
			stats_update.variance = meas_update.Variance();
			stats_update.stdev = meas_update.StandardDeviation();
			stats_update.sterr = meas_update.StandardDeviation() / ((double)sqrt(meas_update.NumDataValues()));

			stats_total.mean = meas_total.Mean();
			stats_total.variance = meas_total.Variance();
			stats_total.stdev = meas_total.StandardDeviation();
			stats_total.sterr = meas_total.StandardDeviation() / ((double)sqrt(meas_total.NumDataValues()));

			fprintf(fp,"%f, %f, %f, %f\n", stats_calc.mean, stats_calc.variance, stats_calc.stdev, stats_calc.sterr);
			fprintf(fp,"%f, %f, %f, %f\n", stats_update.mean, stats_update.variance, stats_update.stdev, stats_update.sterr);
			fprintf(fp,"%f, %f, %f, %f\n", stats_total.mean, stats_total.variance, stats_total.stdev, stats_total.sterr);
			fflush(fp);

			cudaFree(separation);
			cudaFree(cohesion);
			cudaFree(alignement);
			cudaFree(num_A);
			cudaFree(num_B);
			cudaFree(num_C);
			cudaFree(d_displacement);
			cudaFree(d_visited);
			cudaFree(d_vel);
			//cudaFree(d_counter);
			delete h_vel;
			delete m_copy;
			delete m;

			meas_calc_outer.Push(stats_update.mean);
			meas_update_outer.Push(stats_update.mean);
			meas_total_outer.Push(stats_update.mean);
		}

		stats_calc_outer.mean = meas_calc_outer.Mean();
		stats_calc_outer.variance = meas_calc_outer.Variance();
		stats_calc_outer.stdev = meas_calc_outer.StandardDeviation();
		stats_calc_outer.sterr = meas_calc_outer.StandardDeviation() / ((double)sqrt(meas_calc_outer.NumDataValues()));

		stats_update_outer.mean = meas_update_outer.Mean();
		stats_update_outer.variance = meas_update_outer.Variance();
		stats_update_outer.stdev = meas_update_outer.StandardDeviation();
		stats_update_outer.sterr = meas_update_outer.StandardDeviation() / ((double)sqrt(meas_update_outer.NumDataValues()));

		stats_total_outer.mean = meas_total_outer.Mean();
		stats_total_outer.variance = meas_total_outer.Variance();
		stats_total_outer.stdev = meas_total_outer.StandardDeviation();
		stats_total_outer.sterr = meas_total_outer.StandardDeviation() / ((double)sqrt(meas_total_outer.NumDataValues()));

		fprintf(fp,"%f, %f, %f, %f\n", stats_calc_outer.mean, stats_calc_outer.variance, stats_calc_outer.stdev, stats_calc_outer.sterr);
		fprintf(fp,"%f, %f, %f, %f\n", stats_update_outer.mean, stats_update_outer.variance, stats_update_outer.stdev, stats_update_outer.sterr);
		fprintf(fp,"%f, %f, %f, %f\n", stats_total_outer.mean, stats_total_outer.variance, stats_total_outer.stdev, stats_total_outer.sterr);
		fflush(fp);
	}

	return 0;
}