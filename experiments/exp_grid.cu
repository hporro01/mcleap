#include "CLL/particleSystem.h"
#include <string>
#include <fstream>
#include "helper_functions.h"

#include "stats.cuh"

#define WIDTH 900
#define HEIGHT 900

//#define SIM_SCALING 64.0
#define MAX_VEL 10.0

ParticleSystem* build_particle_system_from_off_file(std::string filename, CLL_REAL particleRadius, CLL_uint2 gridSize, float sim_scale) {
	std::ifstream file(filename.c_str(), std::ifstream::in);
	if (!file.is_open()) {
		printf("MCLEAP::FILE \" %s \" NOT FOUND", filename.c_str());
		return nullptr;
	}
	std::string::size_type idx;
	idx = filename.rfind('.');
	if (idx != std::string::npos) {
		std::string extension = filename.substr(idx + 1);
		if (extension != "off") {
			printf("MCLEAP::FILE \" %s \" HAS A WRONG EXTENSION", filename.c_str());
			file.close();
			return nullptr;
		}
	}
	std::string line;
	std::getline(file, line);  // OFF line
	while (true) {
		std::getline(file, line);
		if (line[0] != '#')
			break;
	}
	int num_vertices,num_triangles,num_edges;
	sscanf(line.c_str(), "%d %d %d\n", &num_vertices, &num_triangles, &num_edges);
	ParticleSystem* ps = new ParticleSystem(num_vertices, particleRadius, gridSize);

	for (int i = 0; i < num_vertices; i++) {
		std::getline(file, line);
		CLL_REAL dummy = 0.0;
#ifdef MCLEAP_DOUBLE_PRECISION
		sscanf(line.c_str(), "%lf %lf %lf\n", &ps->m_hPos[i].x, &ps->m_hPos[i].y, &dummy);
#else
		sscanf(line.c_str(), "%f %f %f\n", &ps->m_hPos[i].x, &ps->m_hPos[i].y, &dummy);
#endif
		// Scaling of the system
		ps->m_hPos[i].x *= sim_scale; 
		ps->m_hPos[i].y *= sim_scale;
	}
	file.close();
	
	ps->m_hVel[0].x = 0.0;
	ps->m_hVel[0].y = 0.0;
	ps->m_hVel[1].x = 0.0;
	ps->m_hVel[1].y = 0.0;
	ps->m_hVel[2].x = 0.0;
	ps->m_hVel[2].y = 0.0;
	ps->m_hVel[3].x = 0.0;
	ps->m_hVel[3].y = 0.0;
	for (int i = 4; i < num_vertices; i++) {
		ps->m_hVel[i].x = (i % 2 ? 1 : -1) * ((CLL_REAL)i / num_vertices) * MAX_VEL;
		ps->m_hVel[i].y = (i % 2 ? -1 : 1) * ((CLL_REAL)i / num_vertices) * MAX_VEL;
	}

	ps->transfer_pos_vel_to_gpu();
	return ps;
}

void save_points(std::string filename, ParticleSystem* ps) {
	ps->transfer_pos_vel_to_cpu();
	std::fstream file_out;
	file_out.open(filename, std::ios_base::out);
	file_out << ps->m_numParticles << "\n";
	for (int i = 0; i < ps->m_numParticles; i++) {
		file_out << ((CLL_VEC*)ps->m_hPos)[i].x << " " << ((CLL_VEC*)ps->m_hPos)[i].y << "\n";
	}
	file_out.close();
}

int main() {

	std::string meshes[] = { std::string("../resources/mesh100000.off"), std::string("../resources/mesh_grid_1000000.off") };
	//float max_vels[] = { 2.0, 10.0, 20.0, 30.0, 40.0, 50.0 };
	float max_vel = 30.0;
	float sim_scaling[] = { 64.0, 128.0 };
	float rad_scaling[] = { 10.0 };
	float time_steps[] = { 0.001, 0.0001 };

	int num_realizations = 3;

	FILE* fp;
	char name[50];
	int roll_no, i, n;
	float marks;

	fp = fopen("grid.txt", "w");

	if (fp == NULL) {
		printf("file can't be opened\n");
		exit(1);
	}

	double progress = 0.0;
	for (auto mesh : meshes)for (auto sim_scale : sim_scaling)for (auto rad_scale : rad_scaling)for (auto timestep : time_steps)
	{
		printProgress(progress);
		progress += 1.0 / 8.0; // 1/8 cases
		fprintf(fp, "mesh, sim_scaling, rad_scaling, time_step\n");
		fprintf(fp, "%s, %f, %f, %f\n", mesh.c_str(), sim_scale, rad_scale, timestep);

		fprintf(fp, "Realizations\n%d\n", num_realizations);

		statistics stats_update_outer;
		RunningStat meas_update_outer;

		statistics stats_calc_outer;
		RunningStat meas_calc_outer;

		statistics stats_total_outer;
		RunningStat meas_total_outer;

		for (int REALIZATION = 0; REALIZATION < num_realizations; REALIZATION++)
		{

			statistics stats_update;
			RunningStat meas_update;

			statistics stats_calc;
			RunningStat meas_calc;

			statistics stats_total;
			RunningStat meas_total;

			ParticleSystem* ps = build_particle_system_from_off_file(mesh, sim_scale*40.0, CLL_uint2{ 256, 256 }, sim_scale);
			
		
			int numVertices = ps->m_numParticles;
			CLL_VEC* separation, * cohesion, * alignement;
			int* num_A, * num_B, * num_C;
			cudaMalloc((void**)&separation, numVertices * sizeof(CLL_VEC));
			cudaMalloc((void**)&cohesion, numVertices * sizeof(CLL_VEC));
			cudaMalloc((void**)&alignement, numVertices * sizeof(CLL_VEC));
			cudaMalloc((void**)&num_A, numVertices * sizeof(int));
			cudaMalloc((void**)&num_B, numVertices * sizeof(int));
			cudaMalloc((void**)&num_C, numVertices * sizeof(int));

			ps->num_A = num_A;
			ps->num_B = num_B;
			ps->num_C = num_C;
			ps->separation = separation;
			ps->cohesion = cohesion;
			ps->alignement = alignement;


			//int itercount = 60 * (1.0 / timestep); // in order to make 1 minute of simulation
			int itercount = 100;
			fprintf(fp, "Number of iterations (trace):\n%d\n", itercount);

			for (int i = 0; i < itercount;i++) {
				float diff1, diff2, diff3;
				auto begin1 = std::chrono::high_resolution_clock::now();
				// BUILD
				{
					auto begin = std::chrono::high_resolution_clock::now();
					cudaMemset(separation, 0.0, numVertices * sizeof(CLL_REAL) * 2);
					cudaMemset(cohesion, 0.0, numVertices * sizeof(CLL_REAL) * 2);
					cudaMemset(alignement, 0.0, numVertices * sizeof(CLL_REAL) * 2);
					cudaMemset(num_A, 0, numVertices * sizeof(int));
					cudaMemset(num_B, 0, numVertices * sizeof(int));
					cudaMemset(num_C, 0, numVertices * sizeof(int));
					ps->build(timestep, sim_scale, rad_scale);
					auto end = std::chrono::high_resolution_clock::now();
					auto diff = std::chrono::duration<float, std::milli>(end - begin).count();
					diff1 = diff;
					meas_calc.Push(diff);
				}
				// UPDATE
				{
					auto begin = std::chrono::high_resolution_clock::now();
					ps->update(timestep, sim_scale, rad_scale);
					auto end = std::chrono::high_resolution_clock::now();
					auto diff = std::chrono::duration<float, std::milli>(end - begin).count();
					diff2 = diff;
					meas_update.Push(diff);
				}
				auto end = std::chrono::high_resolution_clock::now();
				auto diff = std::chrono::duration<float, std::milli>(end - begin1).count();
				diff3 = diff;
				fprintf(fp, "%f %f %f\n", diff1, diff2, diff3);
				meas_total.Push(diff);
			}

			stats_calc.mean = meas_calc.Mean();
			stats_calc.variance = meas_calc.Variance();
			stats_calc.stdev = meas_calc.StandardDeviation();
			stats_calc.sterr = meas_calc.StandardDeviation() / ((double)sqrt(meas_calc.NumDataValues()));

			stats_update.mean = meas_update.Mean();
			stats_update.variance = meas_update.Variance();
			stats_update.stdev = meas_update.StandardDeviation();
			stats_update.sterr = meas_update.StandardDeviation() / ((double)sqrt(meas_update.NumDataValues()));

			stats_total.mean = meas_update.Mean();
			stats_total.variance = meas_update.Variance();
			stats_total.stdev = meas_update.StandardDeviation();
			stats_total.sterr = meas_update.StandardDeviation() / ((double)sqrt(meas_update.NumDataValues()));

			fprintf(fp, "%f, %f, %f, %f\n", stats_calc.mean, stats_calc.variance, stats_calc.stdev, stats_calc.sterr);
			fprintf(fp, "%f, %f, %f, %f\n", stats_update.mean, stats_update.variance, stats_update.stdev, stats_update.sterr);
			fprintf(fp, "%f, %f, %f, %f\n", stats_total.mean, stats_total.variance, stats_total.stdev, stats_total.sterr);
			fflush(fp);

			cudaFree(separation);
			cudaFree(cohesion);
			cudaFree(alignement);
			cudaFree(num_A);
			cudaFree(num_B);
			cudaFree(num_C);
			delete ps;

			meas_calc_outer.Push(stats_update.mean);
			meas_update_outer.Push(stats_update.mean);
			meas_total_outer.Push(stats_update.mean);
		}
		stats_calc_outer.mean = meas_calc_outer.Mean();
		stats_calc_outer.variance = meas_calc_outer.Variance();
		stats_calc_outer.stdev = meas_calc_outer.StandardDeviation();
		stats_calc_outer.sterr = meas_calc_outer.StandardDeviation() / ((double)sqrt(meas_calc_outer.NumDataValues()));

		stats_update_outer.mean = meas_update_outer.Mean();
		stats_update_outer.variance = meas_update_outer.Variance();
		stats_update_outer.stdev = meas_update_outer.StandardDeviation();
		stats_update_outer.sterr = meas_update_outer.StandardDeviation() / ((double)sqrt(meas_update_outer.NumDataValues()));

		stats_total_outer.mean = meas_total_outer.Mean();
		stats_total_outer.variance = meas_total_outer.Variance();
		stats_total_outer.stdev = meas_total_outer.StandardDeviation();
		stats_total_outer.sterr = meas_total_outer.StandardDeviation() / ((double)sqrt(meas_total_outer.NumDataValues()));

		fprintf(fp, "%f, %f, %f, %f\n", stats_calc_outer.mean, stats_calc_outer.variance, stats_calc_outer.stdev, stats_calc_outer.sterr);
		fprintf(fp, "%f, %f, %f, %f\n", stats_update_outer.mean, stats_update_outer.variance, stats_update_outer.stdev, stats_update_outer.sterr);
		fprintf(fp, "%f, %f, %f, %f\n", stats_total_outer.mean, stats_total_outer.variance, stats_total_outer.stdev, stats_total_outer.sterr);
		fflush(fp);
	}
	
}