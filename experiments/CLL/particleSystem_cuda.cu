#include <cstdlib>
#include <cstdio>
#include <string.h>

#include <cuda_runtime.h>

#include "thrust/device_ptr.h"
#include "thrust/for_each.h"
#include "thrust/iterator/zip_iterator.h"
#include "thrust/sort.h"

#include "particles_kernel_impl.cuh"

#include <assert.h>
#include <math.h>
#include <memory.h>
#include <cstdio>
#include <cstdlib>
#include <algorithm>

__device__ __host__ CLL_VEC make_VEC(CLL_REAL x, CLL_REAL y){
    return CLL_VEC{ x, y };
}

//Round a / b to nearest higher integer value
uint iDivUp(uint a, uint b)
{
    return (a % b != 0) ? (a / b + 1) : (a / b);
}

// compute grid and thread block size for a given number of elements
void computeGridSize(uint n, uint blockSize, uint& numBlocks, uint& numThreads)
{
    numThreads = min(blockSize, n);
    numBlocks = iDivUp(n, numThreads);
}

void integrateSystem(CLL_VEC* pos,
    CLL_VEC* vel,
    CLL_REAL deltaTime,
    uint numParticles,
    CLL_VEC* separation,
    CLL_VEC* cohesion,
    CLL_VEC* alignement,
    int* num_A,
    int* num_B,
    int* num_C,
    float sim_scale)
{
    uint numThreads, numBlocks;
    computeGridSize(numParticles, 256, numBlocks, numThreads);
    integrate_sim_kernel << <numBlocks, numThreads >> > (pos, vel, deltaTime, numParticles,
        separation,
        cohesion,
        alignement,
        num_A,
        num_B,
        num_C,
        sim_scale);
}

void calcHash(uint* gridParticleHash,
    uint* gridParticleIndex,
    CLL_VEC* pos,
    int    numParticles,
    CLL_uint2 gridSize,
    CLL_VEC cellSize,
    float sim_scale)
{
    uint numThreads, numBlocks;
    computeGridSize(numParticles, 256, numBlocks, numThreads);

    // execute the kernel
    calcHashD << < numBlocks, numThreads >> > (gridParticleHash,
        gridParticleIndex,
        pos,
        numParticles,
        gridSize,
        cellSize,
        sim_scale);
}

void reorderDataAndFindCellStart(uint* cellStart,
    uint* cellEnd,
    CLL_VEC* sortedPos,
    CLL_VEC* sortedVel,
    uint* gridParticleHash,
    uint* gridParticleIndex,
    CLL_VEC* oldPos,
    CLL_VEC* oldVel,
    uint   numParticles,
    uint   numCells)
{
    uint numThreads, numBlocks;
    computeGridSize(numParticles, 256, numBlocks, numThreads);

    uint smemSize = sizeof(uint) * (numThreads + 1);
    reorderDataAndFindCellStartD << < numBlocks, numThreads, smemSize >> > (
        cellStart,
        cellEnd,
        sortedPos,
        sortedVel,
        gridParticleHash,
        gridParticleIndex,
        oldPos,
        oldVel,
        numParticles);
}

void collide(CLL_VEC* newVel,
    CLL_VEC* sortedPos,
    CLL_VEC* sortedVel,
    uint* gridParticleIndex,
    uint* cellStart,
    uint* cellEnd,
    uint   numParticles,
    uint   numCells,
    CLL_REAL particleRadius,
    CLL_uint2 gridSize,
    CLL_VEC cellSize,
    CLL_VEC* separation,
    CLL_VEC* cohesion,
    CLL_VEC* alignement,
    int* num_A,
    int* num_B,
    int* num_C,
    float rad_scale,
    float sim_scale)
{

    // thread per particle
    uint numThreads, numBlocks;
    computeGridSize(numParticles, 64, numBlocks, numThreads);

    // execute the kernel
    collideD << < numBlocks, numThreads >> > (newVel,
        sortedPos,
        sortedVel,
        gridParticleIndex,
        cellStart,
        cellEnd,
        numParticles,
        particleRadius,
        gridSize,
        cellSize,
        separation,
        cohesion,
        alignement,
        num_A,
        num_B,
        num_C,
        rad_scale,
        sim_scale);

}


void sortParticles(uint* dGridParticleHash, uint* dGridParticleIndex, uint numParticles)
{
    thrust::sort_by_key(thrust::device_ptr<uint>(dGridParticleHash),
        thrust::device_ptr<uint>(dGridParticleHash + numParticles),
        thrust::device_ptr<uint>(dGridParticleIndex));
}


ParticleSystem::ParticleSystem(uint numParticles, CLL_REAL particleRadius, CLL_uint2 gridSize) :
    m_bInitialized(false),
    m_numParticles(numParticles),
    m_hPos(0),
    m_hVel(0),
    m_dPos(0),
    m_dVel(0),
    m_gridSize(gridSize),
    m_particleRadius(particleRadius)
{
    m_numGridCells = m_gridSize.x * m_gridSize.y;

    CLL_REAL cellSize = m_particleRadius;  // cell size equal to particle diameter
    m_cellSize = make_VEC(cellSize, cellSize);

    _initialize(numParticles);
}

ParticleSystem::~ParticleSystem()
{
    _finalize();
}


void
ParticleSystem::_initialize(int numParticles)
{
    assert(!m_bInitialized);

    m_numParticles = numParticles;

    // allocate host storage
    m_hPos = new CLL_VEC[m_numParticles];
    m_hVel = new CLL_VEC[m_numParticles];
    memset(m_hPos, 0, m_numParticles * sizeof(CLL_VEC));
    memset(m_hVel, 0, m_numParticles * sizeof(CLL_VEC));

    m_hCellStart = new uint[m_numGridCells];
    memset(m_hCellStart, 0, m_numGridCells * sizeof(uint));

    m_hCellEnd = new uint[m_numGridCells];
    memset(m_hCellEnd, 0, m_numGridCells * sizeof(uint));

    // allocate GPU data
    unsigned int memSize = sizeof(CLL_VEC) * m_numParticles;

    cudaMalloc((void**)&m_dPos, memSize);
    cudaMalloc((void**)&m_dVel, memSize);

    cudaMalloc((void**)&m_dSortedPos, memSize);
    cudaMalloc((void**)&m_dSortedVel, memSize);

    cudaMalloc((void**)&m_dGridParticleHash, m_numParticles * sizeof(uint));
    cudaMalloc((void**)&m_dGridParticleIndex, m_numParticles * sizeof(uint));

    cudaMalloc((void**)&m_dCellStart, m_numGridCells * sizeof(uint));
    cudaMalloc((void**)&m_dCellEnd, m_numGridCells * sizeof(uint));

    m_bInitialized = true;
}

void ParticleSystem::_finalize()
{
    assert(m_bInitialized);

    delete[] m_hPos;
    delete[] m_hVel;
    delete[] m_hCellStart;
    delete[] m_hCellEnd;

    cudaFree(m_dPos);
    cudaFree(m_dVel);
    cudaFree(m_dSortedPos);
    cudaFree(m_dSortedVel);

    cudaFree(m_dGridParticleHash);
    cudaFree(m_dGridParticleIndex);
    cudaFree(m_dCellStart);
    cudaFree(m_dCellEnd);
}

void ParticleSystem::transfer_pos_vel_to_gpu() {
    cudaMemcpy(m_dPos, m_hPos, m_numParticles * sizeof(CLL_VEC), cudaMemcpyHostToDevice);
    cudaMemcpy(m_dVel, m_hVel, m_numParticles * sizeof(CLL_VEC), cudaMemcpyHostToDevice);
}

void ParticleSystem::transfer_pos_vel_to_cpu() {
    cudaDeviceSynchronize();
    cudaMemcpy(m_hPos, m_dPos, m_numParticles * sizeof(CLL_VEC), cudaMemcpyDeviceToHost);
    cudaMemcpy(m_hVel, m_dVel, m_numParticles * sizeof(CLL_VEC), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
}

// step the simulation
void ParticleSystem::update(CLL_REAL deltaTime, float sim_scale, float rad_scale)
{
    assert(m_bInitialized);

    CLL_VEC* dPos = m_dPos;

    // process collisions
    collide(
        m_dVel,
        m_dSortedPos,
        m_dSortedVel,
        m_dGridParticleIndex,
        m_dCellStart,
        m_dCellEnd,
        m_numParticles,
        m_numGridCells,
        m_particleRadius,
        m_gridSize,
        m_cellSize,
        separation,
        cohesion,
        alignement,
        num_A,
        num_B,
        num_C,
        rad_scale,
        sim_scale);
    CudaCheckError();

    // integrate
    integrateSystem(
        dPos,
        m_dVel,
        deltaTime,
        m_numParticles,
        separation,
        cohesion,
        alignement,
        num_A,
        num_B,
        num_C,
        sim_scale);
    CudaCheckError();
    cudaDeviceSynchronize();
}

void ParticleSystem::build(CLL_REAL deltaTime, float sim_scale, float rad_scale)
{
    assert(m_bInitialized);

    CLL_VEC* dPos = m_dPos;

    CudaCheckError();
    // calculate grid hash
    calcHash(
        m_dGridParticleHash,
        m_dGridParticleIndex,
        dPos,
        m_numParticles,
        m_gridSize,
        m_cellSize,
        sim_scale);
    CudaCheckError();

    // sort particles based on hash
    sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles);
    CudaCheckError();

    // reorder particle arrays into sorted order and
    // find start and end of each cell
    reorderDataAndFindCellStart(
        m_dCellStart,
        m_dCellEnd,
        m_dSortedPos,
        m_dSortedVel,
        m_dGridParticleHash,
        m_dGridParticleIndex,
        dPos,
        m_dVel,
        m_numParticles,
        m_numGridCells);
    CudaCheckError();
    cudaDeviceSynchronize();
}
