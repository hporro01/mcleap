#ifndef __PARTICLESYSTEM_H__
#define __PARTICLESYSTEM_H__

template<class T>
struct vec2 {
    T x, y;
    __host__ __device__ inline T& operator[](const int& i) { return ((T*)this)[i]; }
};
template<class T>
__host__ __device__ inline vec2<T> operator-(const vec2<T>& v, const vec2<T>& q) {
    return vec2<T>{v.x - q.x, v.y - q.y};
}
template<class T>
__host__ __device__ inline vec2<T> operator+(const vec2<T>& v, const vec2<T>& q) {
    return vec2<T>{v.x + q.x, v.y + q.y};
}
template<class T>
__host__ __device__ inline vec2<T> operator/(const vec2<T>& v, const T& a) {
    return vec2<T>{v.x / a, v.y / a};
}
template<class T>
__device__ T length(vec2<T>& v) {
    return sqrt(v.x * v.x + v.y * v.y);
}
template<class T>
__device__ vec2<T> normalize(vec2<T>& v) {
    return v / sqrt(v.x * v.x + v.y * v.y);
}

using uint = unsigned int;
#ifdef MCLEAP_DOUBLE_PRECISION
using CLL_REAL = double;
using CLL_VEC = vec2<double>;
#else
using CLL_REAL = float;
using CLL_VEC = vec2<float>;
#endif
using CLL_int2 = vec2<int>;
using CLL_uint2 = vec2<unsigned int>;

__device__ __host__ CLL_VEC make_VEC(CLL_REAL x, CLL_REAL y);

// Particle system class
class ParticleSystem {
public:

    ParticleSystem(uint numParticles, CLL_REAL particleRadius, CLL_uint2 gridSize);
    ~ParticleSystem();

    void build(CLL_REAL deltaTime, float sim_scale, float rad_scale);
    void update(CLL_REAL deltaTime, float sim_scale, float rad_scale);

    void _initialize(int numParticles);
    void _finalize();
    void transfer_pos_vel_to_gpu();
    void transfer_pos_vel_to_cpu();

    bool m_bInitialized;
    uint m_numParticles;

    // CPU data
    CLL_VEC *m_hPos;              // particle positions
    CLL_VEC *m_hVel;              // particle velocities

    uint  *m_hParticleHash;
    uint  *m_hCellStart;
    uint  *m_hCellEnd;

    // GPU data
    CLL_VEC *m_dPos;
    CLL_VEC *m_dVel;

    CLL_VEC *m_dSortedPos;
    CLL_VEC *m_dSortedVel;

    // grid data for sorting method
    uint  *m_dGridParticleHash; // grid hash value for each particle
    uint  *m_dGridParticleIndex;// particle index for each particle
    uint  *m_dCellStart;        // index of start of each cell in sorted list
    uint  *m_dCellEnd;          // index of end of cell

    // params
    CLL_uint2 m_gridSize;
    uint m_numGridCells;

    // new
    CLL_VEC m_cellSize;
    CLL_REAL m_particleRadius;

    // all this are gpu data
    CLL_VEC* separation;
    CLL_VEC* cohesion;
    CLL_VEC* alignement;
    int* num_A;
    int* num_B;
    int* num_C;


};

#endif // __PARTICLESYSTEM_H__
