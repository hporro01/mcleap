#ifndef _PARTICLES_KERNEL_H_
#define _PARTICLES_KERNEL_H_

#include <stdio.h>
#include <math.h>
#include "../helper_functions.cuh"
#include "particleSystem.h"

//#define SIM_SCALING 64.0
#define MAX_VEL 30.0

#define RADA 14.0
#define RADB 20.0
#define RADC 40.0

#define A_FORCE 100.0
#define B_FORCE 5.0
#define C_FORCE 40.0

__device__ CLL_REAL kernel_walls(CLL_REAL x) {
    return -5 * exp(-0.01 * x * x + 4);
}

__device__ CLL_REAL clamp(CLL_REAL a, CLL_REAL b, CLL_REAL c) {
    if (a < b)return b;
    if (a > c)return c;
    return a;
}

__global__ void integrate_sim_kernel(CLL_VEC* pos,
                                     CLL_VEC* vel,
                                     CLL_REAL deltaTime,
                                     uint numParticles,
                                     CLL_VEC* separation, 
                                     CLL_VEC* cohesion, 
                                     CLL_VEC* alignement, 
                                     int* num_A, 
                                     int* num_B, 
                                     int* num_C,
                                     float sim_scale)
{
    unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < numParticles && i>3) {
        if (num_A[i] > 0)separation[i].x = (separation[i].x * A_FORCE) / (CLL_REAL)num_A[i];
        if (num_A[i] > 0)separation[i].y = (separation[i].y * A_FORCE) / (CLL_REAL)num_A[i];

        if (num_B[i] > 0)cohesion[i].x = (cohesion[i].x * B_FORCE) / (CLL_REAL)num_B[i];
        if (num_B[i] > 0)cohesion[i].y = (cohesion[i].y * B_FORCE) / (CLL_REAL)num_B[i];

        if (num_C[i] > 0)alignement[i].x = (alignement[i].x * C_FORCE) / (CLL_REAL)num_C[i];
        if (num_C[i] > 0)alignement[i].y = (alignement[i].y * C_FORCE) / (CLL_REAL)num_C[i];

        CLL_VEC acel = make_VEC(separation[i].x + cohesion[i].x + alignement[i].x, separation[i].y + cohesion[i].y + alignement[i].y );

        CLL_REAL box_size = sim_scale * 100.0;

        acel.x -= kernel_walls(pos[i].x + box_size);
        acel.x += kernel_walls(pos[i].x - box_size);
        acel.y -= kernel_walls(pos[i].y + box_size);
        acel.y += kernel_walls(pos[i].y - box_size);

        vel[i].x += acel.x * deltaTime;
        vel[i].y += acel.y * deltaTime;

        vel[i].x = clamp(vel[i].x, (CLL_REAL)-MAX_VEL, (CLL_REAL)MAX_VEL);
        vel[i].y = clamp(vel[i].y, (CLL_REAL)-MAX_VEL, (CLL_REAL)MAX_VEL);

        pos[i].x += vel[i].x * deltaTime;
        pos[i].y += vel[i].y * deltaTime;
        //printf("pos: %f %f, vel: %f %f\n", pos[i].x, pos[i].y, vel[i].x, vel[i].y);
    }
}

// calculate position in uniform grid
__device__ CLL_int2 calcGridPos(CLL_VEC p, CLL_VEC cellSize, float sim_scale)
{
    CLL_int2 gridPos;
    gridPos.x = floor((p.x + sim_scale * 100.0) / cellSize.x);
    gridPos.y = floor((p.y + sim_scale * 100.0) / cellSize.y);
    return gridPos;
}

// calculate address in grid from position (clamping to edges)
__device__ uint calcGridHash(CLL_int2 gridPos, CLL_uint2 gridSize, CLL_VEC cellSize)
{
    gridPos.x = gridPos.x & (gridSize.x-1);  // wrap grid, assumes size is power of 2
    gridPos.y = gridPos.y & (gridSize.y-1);
    //printf("%d\n", __umul24(gridSize.y, gridSize.x) + gridPos.x);
    return gridPos.y * gridSize.x + gridPos.x;
}

// calculate grid hash value for each particle
__global__
void calcHashD(uint   *gridParticleHash,  // output
               uint   *gridParticleIndex, // output
               CLL_VEC *pos,               // input: positions
               uint    numParticles,
               CLL_uint2 gridSize,
               CLL_VEC cellSize,
               float sim_scale)
{
    uint index = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;

    if (index >= numParticles) return;

    volatile CLL_VEC p = pos[index];

    //if (index == 1234)printf("pos: %f %f\n", p.x, p.y);

    // get address in grid
    CLL_int2 gridPos = calcGridPos(make_VEC(p.x, p.y),cellSize, sim_scale);
    uint hash = calcGridHash(gridPos,gridSize,cellSize);

    // store grid hash and particle index
    gridParticleHash[index] = hash;
    gridParticleIndex[index] = index;
}

// rearrange particle data into sorted order, and find the start of each cell
// in the sorted hash array
__global__
void reorderDataAndFindCellStartD(uint   *cellStart,        // output: cell start index
                                  uint   *cellEnd,          // output: cell end index
                                  CLL_VEC *sortedPos,        // output: sorted positions
                                  CLL_VEC *sortedVel,        // output: sorted velocities
                                  uint   *gridParticleHash, // input: sorted grid hashes
                                  uint   *gridParticleIndex,// input: sorted particle indices
                                  CLL_VEC *oldPos,           // input: sorted position array
                                  CLL_VEC *oldVel,           // input: sorted velocity array
                                  uint    numParticles)
{
    extern __shared__ uint sharedHash[];    // blockSize + 1 elements
    uint index = __umul24(blockIdx.x,blockDim.x) + threadIdx.x;

    uint hash;

    // handle case when no. of particles not multiple of block size
    if (index < numParticles)
    {
        hash = gridParticleHash[index];

        // Load hash data into shared memory so that we can look
        // at neighboring particle's hash value without loading
        // two hash values per thread
        sharedHash[threadIdx.x+1] = hash;

        if (index > 0 && threadIdx.x == 0)
        {
            // first thread in block must load neighbor particle hash
            sharedHash[0] = gridParticleHash[index-1];
        }
    }

    __syncthreads();

    if (index < numParticles)
    {
        // If this particle has a different cell index to the previous
        // particle then it must be the first particle in the cell,
        // so store the index of this particle in the cell.
        // As it isn't the first particle, it must also be the cell end of
        // the previous particle's cell

        if (index == 0 || hash != sharedHash[threadIdx.x])
        {
            cellStart[hash] = index;

            if (index > 0)
                cellEnd[sharedHash[threadIdx.x]] = index;
        }

        if (index == numParticles - 1)
        {
            cellEnd[hash] = index + 1;
        }

        // Now use the sorted index to reorder the pos and vel data
        uint sortedIndex = gridParticleIndex[index];
        CLL_VEC pos = oldPos[sortedIndex];       // macro does either global read or texture fetch
        CLL_VEC vel = oldVel[sortedIndex];       // see particles_kernel.cuh

        sortedPos[index] = pos;
        sortedVel[index] = vel;
    }


}

//__device__ CLL_VEC operator-(CLL_VEC a, CLL_VEC b) {
//    return make_VEC(a.x-b.x,a.y-b.y);
//}

// collide two spheres using DEM method
__device__
void collideSpheres(CLL_VEC  posA, CLL_VEC  posB,
                      CLL_VEC  velA, CLL_VEC  velB,
                      CLL_REAL radiusA, CLL_REAL radiusB,
                      CLL_VEC * separation, 
                      CLL_VEC * cohesion,
                      CLL_VEC * alignement,
                      int* num_A,
                      int* num_B,
                      int* num_C,
                      int i, int j,
                      float rad_scale)
{
    // calculate relative position
    CLL_VEC relPos = posB - posA;

    CLL_REAL dist = length(relPos);
    CLL_REAL collideDist = radiusA; // + radiusB for collisions


    if (dist < collideDist * rad_scale)
    {
        //if (i == 1234)/*printf*/("Collide: %d %d %f\n", i, j, dist);
        if (dist <= RADA * rad_scale) {
            separation[i].x += relPos.x;
            separation[i].y += relPos.y;
            num_A[i] += 1;
        }
        if (dist <= RADB * rad_scale) {
            cohesion[i].x += relPos.x;
            cohesion[i].y += relPos.y;
            num_B[i] += 1;
        }
        if (dist <= RADC * rad_scale) {
            CLL_VEC norm_vel = normalize(velA);
            alignement[i].x += norm_vel.x;
            alignement[i].y += norm_vel.y;
            num_C[i] += 1;
        }
    }
}



// collide a particle against all other particles in a given cell
__device__
void collideCell(  CLL_int2    gridPos,
                   uint    index,
                   CLL_VEC  pos,
                   CLL_VEC  vel,
                   CLL_VEC *oldPos,
                   CLL_VEC *oldVel,
                   uint   *cellStart,
                   uint   *cellEnd,
                   CLL_REAL particleRadius,
                   CLL_uint2 gridSize,
                   CLL_VEC cellSize,
                   CLL_VEC* separation,
                   CLL_VEC* cohesion,
                   CLL_VEC* alignement,
                   int* num_A,
                   int* num_B,
                   int* num_C,
                   uint* gridParticleIndex,
                   float rad_scale
                )
{
    uint gridHash = calcGridHash(gridPos,gridSize,cellSize);

    // get start of bucket for this cell
    uint startIndex = cellStart[gridHash];

    if (startIndex != 0xffffffff)          // cell is not empty
    {
        // iterate over particles in this cell
        uint endIndex = cellEnd[gridHash];

        for (uint j=startIndex; j<endIndex; j++)
        {
            if (j != index)                // check not colliding with self
            {
                CLL_VEC pos2 = make_VEC(oldPos[j].x,oldPos[j].y);
                CLL_VEC vel2 = make_VEC(oldVel[j].x,oldVel[j].y);

                // collide two spheres
                collideSpheres(pos, pos2, vel, vel2, particleRadius, particleRadius, separation,
                    cohesion,
                    alignement,
                    num_A,
                    num_B,
                    num_C, gridParticleIndex[index], gridParticleIndex[j], rad_scale);
                //gridParticleIndex[index];
                //gridParticleIndex[j];
            }
        }
    }
}


__global__
void collideD(CLL_VEC *newVel,               // output: new velocity
              CLL_VEC *oldPos,               // input: sorted positions
              CLL_VEC *oldVel,               // input: sorted velocities
              uint   *gridParticleIndex,    // input: sorted particle indices
              uint   *cellStart,
              uint   *cellEnd,
              uint    numParticles,
              CLL_REAL particleRadius,
              CLL_uint2 gridSize,
              CLL_VEC cellSize,
              CLL_VEC* separation,
              CLL_VEC* cohesion,
              CLL_VEC* alignement,
              int* num_A,
              int* num_B,
              int* num_C,
              float rad_scale,
              float sim_scale
    )
{
    uint index = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;

    if (index >= numParticles) return;

    // read particle data from sorted arrays
    CLL_VEC pos = make_VEC(oldPos[index].x,oldPos[index].y);
    CLL_VEC vel = make_VEC(oldVel[index].x,oldPos[index].y);

    // get address in grid
    CLL_int2 gridPos = calcGridPos(pos,cellSize, sim_scale);

    // examine neighbouring cells

    for (int y=-1; y<=1; y++)
    {
        for (int x=-1; x<=1; x++)
        {
            CLL_int2 neighbourPos = gridPos + CLL_int2{x, y};
            collideCell(neighbourPos, index, pos, vel, oldPos, oldVel, cellStart, cellEnd, particleRadius, gridSize, cellSize, separation, cohesion, alignement, num_A, num_B, num_C, gridParticleIndex, rad_scale);
        }
    }

    // write new velocity back to original unsorted location
    uint originalIndex = gridParticleIndex[index];
    newVel[originalIndex] = make_VEC(vel.x,vel.y);
}


#endif
