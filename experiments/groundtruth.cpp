#include <stdio.h>
#include <string>
#include <fstream>
#include <math.h>

#define SIM_SCALING 64.0
#define INITIAL_SIM_MESH "../resources/mesh100000.off"
#define RAD 40.0

struct part_system {
	float* pos;
	int num_particles;
	part_system(int num_particles) : num_particles(num_particles) {
		pos = new float[num_particles * 2];
	}
	~part_system() {
		delete[] pos;
	}
};

part_system* read_off_positions(std::string filename) {
	std::ifstream file(filename.c_str(), std::ifstream::in);
	if (!file.is_open()) {
		printf("MCLEAP::FILE \" %s \" NOT FOUND", filename.c_str());
		return nullptr;
	}
	std::string::size_type idx;
	idx = filename.rfind('.');
	if (idx != std::string::npos) {
		std::string extension = filename.substr(idx + 1);
		if (extension != "off") {
			printf("MCLEAP::FILE \" %s \" HAS A WRONG EXTENSION", filename.c_str());
			file.close();
			return nullptr;
		}
	}
	std::string line;
	std::getline(file, line);  // OFF line
	while (true) {
		std::getline(file, line);
		if (line[0] != '#')
			break;
	}
	int num_vertices, num_triangles, num_edges;
	sscanf(line.c_str(), "%d %d %d\n", &num_vertices, &num_triangles, &num_edges);
	part_system* ps = new part_system(num_vertices);

	for (int i = 0; i < num_vertices; i++) {
		std::getline(file, line);
		float dummy = 0.0;
		sscanf(line.c_str(), "%f %f %f\n", &ps->pos[i * 2 + 0], &ps->pos[i * 2 + 1], &dummy);
		// Scaling of the system
		ps->pos[i * 2 + 0] *= SIM_SCALING;
		ps->pos[i * 2 + 1] *= SIM_SCALING;
	}
	file.close();

	return ps;
}

struct Vec {
	float x, y;
	float& operator[](const int& i) {
		return ((float*)this)[i];
	}
};

Vec operator-(Vec a, Vec b) {
	return Vec{ a.x - b.x, a.y - b.y };
}

float pow2(float a) {
	return a * a;
}

float dist(Vec a, Vec b) {
	Vec ab = a - b;
	return sqrt(pow2(ab.x) + pow2(ab.y));
}

int main() {
	std::string filename(INITIAL_SIM_MESH);
	part_system* ps = read_off_positions(filename);
	if (ps == nullptr)return -1;
	int i = 1234;
	Vec a{ ps->pos[i * 2 + 0],ps->pos[i * 2 + 1] };

	for (int j = 0; j < ps->num_particles; j++) {
		if (i == j)continue;
		Vec b{ ps->pos[j * 2 + 0],ps->pos[j * 2 + 1] };
		if (dist(a, b) <= RAD) {
			printf("%d %d %f\n", i, j, dist(a, b));
		}
	}
	return 0;
}