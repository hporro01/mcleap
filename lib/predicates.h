#ifdef __cplusplus
extern "C" {
#endif
void exactinit();
double orient2d(double* a, double* b, double* c);
double incircle(double* a, double* b, double* c, double* d);
#ifdef __cplusplus
}
#endif