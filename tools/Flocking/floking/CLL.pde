

// E should be an ArrayList of elements with a pos method
class CLL<E> {
  CLL(float rad, float min_x, float max_x, float min_y, float max_y){
    this.rad = rad;
    this.min_x = min_x;
    this.max_x = max_x;
    this.min_y = min_y;
    this.max_y = max_y;
  }
  public float rad, min_x, max_x, min_y, max_y;
}
