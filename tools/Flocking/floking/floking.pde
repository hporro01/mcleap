int boids_number = 1000;
float radA = 20.0;
float radB = 30.0;
float radC = 40.0;
float a = 30.0;
float b = 5.0;
float c = 20.0;

float max_vel = 70.0;

PVector rot2d(PVector vec, float thetha){
  return new PVector(vec.x*cos(thetha)-vec.y*sin(thetha),vec.y*cos(thetha)+vec.x*sin(thetha));
}

PVector translate2d(PVector vec, PVector dp){
  return PVector.add(vec,dp);
}

PVector scale2d(PVector vec, float xscale, float yscale){
  return new PVector(vec.x*xscale, vec.y*yscale);
}

PVector p1 = new PVector(-1,-1);
PVector p2 = new PVector(1,-1);
PVector p3 = new PVector(0,2);

class Boid {
  public Boid(){
    pos = new PVector(random(100,width-100),random(100,height-100));
    vel = new PVector(random(-max_vel,max_vel),random(-max_vel,max_vel));
    acel = new PVector(0.0,0.0);
    col = new PVector(random(0.0,1.0),random(0.0,1.0),random(0.0,1.0));
  }
  public void draw(){
    stroke(col.x,col.y,col.z);
    direction = atan2(vel.y, vel.x)-PI/2.0;
    PVector v1 = translate2d(scale2d(rot2d(p1,direction),2.0,2.0),pos);
    PVector v2 = translate2d(scale2d(rot2d(p2,direction),2.0,2.0),pos);
    PVector v3 = translate2d(scale2d(rot2d(p3,direction),2.0,2.0),pos);
    line(v1.x,v1.y,v2.x,v2.y);
    line(v2.x,v2.y,v3.x,v3.y);
    line(v3.x,v3.y,v1.x,v1.y);
  }
  public PVector pos, vel, acel;
  public PVector col;
  public float direction;
}

Boid[] Boids = new Boid[boids_number];

void setup(){
  size(1000, 800, P2D);
  for(int i=0;i<boids_number;i++){
    Boids[i] = new Boid();
  }
}

float kernel_walls(float x){
  return -10*exp(-0.001*x*x+4);
}

void draw(){
  background(153);
  float dt = 1.0/frameRate;

  
  PVector[] separation = new PVector[boids_number];
  int[] neighbors_in_A = new int[boids_number];
  PVector[] cohesion = new PVector[boids_number];
  int[] neighbors_in_B = new int[boids_number];
  PVector[] alignement = new PVector[boids_number];
  int[] neighbors_in_C = new int[boids_number];
  
  for(int i=0;i<boids_number;i++){
    separation[i] = new PVector(0.0,0.0);
    neighbors_in_A[i] = 0;
    cohesion[i] = new PVector(0.0,0.0);
    neighbors_in_B[i] = 0;
    alignement[i] = new PVector(0.0,0.0);
    neighbors_in_C[i] = 0;
    for(int j=0;j<boids_number;j++){
      PVector sub_vector = PVector.sub(Boids[j].pos,Boids[i].pos);
      float dist = sub_vector.mag();
      if(dist<=radA){
        separation[i].sub(sub_vector);
        neighbors_in_A[i]+=1;
      }
      else if(dist<=radB){
        cohesion[i].add(sub_vector);
        neighbors_in_B[i]+=1;
      }
      if(dist<=radC){
        PVector norm_vel = PVector.div(Boids[j].vel,Boids[j].vel.mag());
        alignement[i].add(norm_vel);
        neighbors_in_C[i]+=1;
      }
    }
    if(neighbors_in_A[i]>0)separation[i].div(neighbors_in_A[i]).mult(a);
    if(neighbors_in_B[i]>0)cohesion[i].div(neighbors_in_B[i]).mult(b);
    if(neighbors_in_C[i]>0)alignement[i].div(neighbors_in_C[i]).mult(c);
  }
  for(int i=0;i<boids_number;i++){
    Boids[i].acel = PVector.add(separation[i],PVector.add(cohesion[i],alignement[i]));
    
    if(Boids[i].pos.x<width-Boids[i].pos.x){
      Boids[i].acel.x -= kernel_walls(Boids[i].pos.x);
    }
    else{
      Boids[i].acel.x += kernel_walls(width-Boids[i].pos.x);
    }
    if(Boids[i].pos.y<height-Boids[i].pos.y){
      Boids[i].acel.y -= kernel_walls(Boids[i].pos.y);
    }
    else{
      Boids[i].acel.y += kernel_walls(height-Boids[i].pos.y);
    }

    Boids[i].vel.add(PVector.mult(Boids[i].acel,dt));
    if(Boids[i].vel.x<0)
      Boids[i].vel.x = max(-max_vel,Boids[i].vel.x);
    if(Boids[i].vel.x>0)
      Boids[i].vel.x = min(max_vel,Boids[i].vel.x);
    if(Boids[i].vel.y<0)
      Boids[i].vel.y = max(-max_vel,Boids[i].vel.y);
    if(Boids[i].vel.y>0)
      Boids[i].vel.y = min(max_vel,Boids[i].vel.y);
    Boids[i].pos.add(PVector.mult(Boids[i].vel,dt));
    Boids[i].draw();
  }
  println(frameRate);
}
