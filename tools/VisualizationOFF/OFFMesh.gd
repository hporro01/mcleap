extends Node2D

export var OFFfile : String = "res://OFFfiles/log2040.off"
#export var OFFfile : String = "res://logmesh.off"
export var circle_size : float = 0.2
export var global_scaling : float = 40.0

var vertices = []
var triangles = []
var num_vertices : int = -1
var num_triangles : int = -1

func readOFF(filename : String) -> void:
	var file = File.new()
	file.open(filename, File.READ)
	if not file.is_open():
		print("Error, file not found")
		file.close()
		return
	var line = file.get_line()
	line = file.get_line()
	var nums = line.split(' ')
	num_vertices = int(nums[0])
	num_triangles = int(nums[1])
	for i in range(num_vertices):
		line = file.get_line()
		var pos = line.split(' ')
		vertices.append(global_scaling*Vector2(float(pos[0]),-float(pos[1]))) # y is reverted because the y axis is going downwards in godot
	for i in range(num_triangles):
		line = file.get_line()
		var indices = line.split(' ')
		triangles.append([int(indices[1]),int(indices[2]),int(indices[3])])
	file.close()
	
func _ready():
	readOFF(OFFfile)

func _process(delta):
	pass

func _draw():
	for i in range(num_triangles):
		var indices = triangles[i]
		for j in range(3):
			draw_line(vertices[indices[j]],vertices[indices[(j+1)%3]],Color(0.2,0.7,2.0,1.0))
	for i in range(num_vertices):
		draw_circle(vertices[i],circle_size,Color(1.0,0.0,0.0,1.0))
