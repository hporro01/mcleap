extends Camera2D


export var move_vel : float = 2160.0
export var zoom_vel : float = 10.0

func _ready():
	self.position.x += 3900
	self.position.y += 1275

func _physics_process(delta):
	if Input.is_action_pressed("Left"):
		self.position.x -= move_vel*delta
	if Input.is_action_pressed("Right"):
		self.position.x += move_vel*delta
	if Input.is_action_pressed("Down"):
		self.position.y += move_vel*delta
	if Input.is_action_pressed("Up"):
		self.position.y -= move_vel*delta
	if Input.is_action_pressed("ZommIn"):
		self.zoom += Vector2(zoom_vel*delta,zoom_vel*delta)
	if Input.is_action_pressed("ZoomOut"):
		self.zoom -= Vector2(zoom_vel*delta,zoom_vel*delta)
