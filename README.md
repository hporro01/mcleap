## Usage

Most required libraries will be compiled together with the proyect when you run it.

### Usage (Linux) : 
---------------
Before using the library, you have to install some prerequisites:

### Ubuntu
```bash
sudo apt-get install cmake libxrandr-dev libxinerama-dev libxcursor-dev libxi-dev
```

You should also have CUDA installed and available in the path in order to be found by cmake.

Open the directory into a terminal, make a `build` folder. Then run the `CMakeLists.txt` from the `build` folder, build the project using `make` and run one of the examples.

```bash
mkdir build
cd build
cmake ..
make
./examples/boids_example
```

### Usage (Windows) :
-----------------
Open the folder with Visual Studio (2017 or 2019) and it will recognize that there is a `CMakeLists.txt` file. Run the `CMakeLists.txt` file. Finally, run the executable `boids_example.exe`

### Usage, changing the parameters of the visualization


### Usage, experiments
-----------------
Here we are assuming you are using linux to run the experiments.

In order to rerun the experiments showed in the paper you have to set some flags in the `CMakeLists.txt` file. The flags `BUILD_MCLEAP_WITH_OPENGL` and `BUILD_MCLEAP_EXPERIMENTS` have to be set as follows:

```bash
set(BUILD_MCLEAP_WITH_OPENGL off) #Set this variable to off to compile code without OpenGL. When this is off this sets the compile variable MCLEAP_WITH_OPENGL
set(BUILD_MCLEAP_EXPERIMENTS on) #Set this variable off to just compile the library without experiments. Assumes BUILD_MCLEAP_WITH_OPENGL off
```

After re-running cmake and make commands, you can run the files `./experiments/exp_grid` and `./experiments/exp_boids`, to check out the time it takes to run a boids simulation using the grid method or the Delaunay based one respectively. 

## Documentation
-----------------
Doc pages can be generated using Doxygen with cmake.

## Screenshots:
-----------------
![100kPoints](./screenshots/smol_middle_one.PNG)