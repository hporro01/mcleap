#version 420 core

in layout (location = 0) vec2 aPos;

uniform float u_zoom;
out vec2 gl_Position;

void main()
{
    gl_Position = vec2(aPos.x*u_zoom,aPos.y*u_zoom);
}
