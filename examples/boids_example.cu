#ifdef BUILD_MCLEAP_WITH_OPENGL
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Shader.hpp"
#include "glError.hpp"
#endif

#include <iostream>
#include <string>
#include <mcleap_mesh.cuh>
#include <MCleap.h>
#include <frnn_kernels.cuh>

#include <random>

#include "helper_functions.cuh"
#include "helper_functions.h"

#define INITIAL_SIM_MESH "../resources/mesh10000.off"
#define SIM_SCALING 16.0
#define MAX_VEL 30.0

#define SAVING_STATES 0

#define WIDTH 900
#define HEIGHT 900

#define RADIUS 0.5

#define RADA 14.0
#define RADB 20.0
#define RADC 40.0

#define A_FORCE 100.0
#define B_FORCE 5.0
#define C_FORCE 40.0

using MCleap::MCLEAP_VEC;
using MCleap::MCLEAP_REAL;

struct calcForces_functor {
	calcForces_functor(int numVertices, MCLEAP_VEC* vel, MCLEAP_VEC* separation, MCLEAP_VEC* cohesion, MCLEAP_VEC* alignement, int* num_A, int* num_B, int* num_C) :
		numVertices(numVertices), vel(vel), separation(separation), cohesion(cohesion), alignement(alignement), num_A(num_A), num_B(num_B), num_C(num_C) {}
	void resetForces() {
		cudaMemset(separation, 0.0, numVertices * sizeof(MCLEAP_REAL) * 2);
		cudaMemset(cohesion, 0.0, numVertices * sizeof(MCLEAP_REAL) * 2);
		cudaMemset(alignement, 0.0, numVertices * sizeof(MCLEAP_REAL) * 2);
		cudaMemset(num_A, 0, numVertices * sizeof(int));
		cudaMemset(num_B, 0, numVertices * sizeof(int));
		cudaMemset(num_C, 0, numVertices * sizeof(int));
	}
	__device__ void operator()(const int& i, const int& j, MCLEAP_VEC& dist_vec, double &dist) {
		//if(i==1234)printf("Calc: %d %d %lf\n", i, j,dist);
		if (dist <= RADA) {
			separation[i].x += dist_vec.x;
			separation[i].y += dist_vec.y;
			num_A[i] += 1;
		}
		if (dist <= RADB) {
			cohesion[i].x += dist_vec.x;
			cohesion[i].y += dist_vec.y;
			num_B[i] += 1;
		}
		if (dist <= RADC) {
			MCLEAP_VEC norm_vel = vel[j]/(MCLEAP_REAL)mcleap_d_magnitude(vel[j]);
			alignement[i].x += norm_vel.x;
			alignement[i].y += norm_vel.y;
			num_C[i] += 1;
		}
	}
	int numVertices;
	MCLEAP_VEC* vel;
	MCLEAP_VEC* separation, * cohesion, * alignement;
	int* num_A, * num_B, * num_C;
};


__global__ void move_mesh_kernel(int numVertices, MCLEAP_VEC* vertexData, MCLEAP_VEC* velocity, const float dt, MCLEAP_VEC* separation, MCLEAP_VEC* cohesion, MCLEAP_VEC* alignement, int* num_A, int* num_B, int* num_C, MCLEAP_VEC* displacement) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < numVertices) {
		if (num_A[i] > 0)separation[i].x = (separation[i].x * A_FORCE) / (MCLEAP_REAL)num_A[i];
		if (num_A[i] > 0)separation[i].y = (separation[i].y * A_FORCE) / (MCLEAP_REAL)num_A[i];

		if (num_B[i] > 0)cohesion[i].x = (cohesion[i].x * B_FORCE) / (MCLEAP_REAL)num_B[i];
		if (num_B[i] > 0)cohesion[i].y = (cohesion[i].y * B_FORCE) / (MCLEAP_REAL)num_B[i];

		if (num_C[i] > 0)alignement[i].x = (alignement[i].x * C_FORCE) / (MCLEAP_REAL)num_C[i];
		if (num_C[i] > 0)alignement[i].y = (alignement[i].y * C_FORCE) / (MCLEAP_REAL)num_C[i];

		MCLEAP_VEC acel = MCLEAP_VEC{ separation[i].x + cohesion[i].x + alignement[i].x, separation[i].y + cohesion[i].y + alignement[i].y };

		constexpr MCLEAP_REAL box_size = SIM_SCALING*100.0;

		acel.x -= kernel_walls(vertexData[i].x + box_size);
		acel.x += kernel_walls(vertexData[i].x - box_size);
		acel.y -= kernel_walls(vertexData[i].y + box_size);
		acel.y += kernel_walls(vertexData[i].y - box_size);

		velocity[i].x += acel.x * dt;
		velocity[i].y += acel.y * dt;

		velocity[i].x = clamp(velocity[i].x, -MAX_VEL, MAX_VEL);
		velocity[i].y = clamp(velocity[i].y, -MAX_VEL, MAX_VEL);

		displacement[i].x = velocity[i].x * dt;
		displacement[i].y = velocity[i].y * dt;
	}
}


__global__ void avoid_interpenetration_kernel(int numVertices, MCLEAP_VEC* vertexData, MCLEAP_VEC* velocity, int numEdges, MCleap::edge* hed) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < numEdges && (hed[i].tb.x != -1)) {
		int id0 = hed[i].v.x;
		int id1 = hed[i].v.y;
		MCLEAP_VEC v0 = vertexData[id0] + MCLEAP_VEC{ velocity[id0].x,velocity[id0].y };
		MCLEAP_VEC v1 = vertexData[id1] + MCLEAP_VEC{ velocity[id1].x,velocity[id1].y };

		if (dist(v0, v1) < RADIUS) {
			velocity[id0].x *= -1;
			velocity[id0].y *= -1;
			velocity[id1].x *= -1;
			velocity[id1].y *= -1;
		}
	}
}

__global__ void scale_mesh_kernel(int numVertices, MCLEAP_VEC* vertexData, float scale) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < numVertices) {
		vertexData[i].x *= scale;
		vertexData[i].y *= scale;
	}
}

void avoid_interpenetration(MCleap::mcleap_mesh* m, MCLEAP_VEC* velocity) {
	MCleap::map_cuda_buffers(m);

	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_edges + block_size - 1) / dimBlock.x);

	avoid_interpenetration_kernel << <dimGrid, dimBlock >> > (m->num_vertices, m->d_vbo_v, velocity, m->num_edges, m->d_mesh->he);
}

void move_mesh(MCleap::mcleap_mesh* m, MCLEAP_VEC* velocity, const float dt, MCLEAP_VEC* separation, MCLEAP_VEC* cohesion, MCLEAP_VEC* alignement, int* num_A, int* num_B, int* num_C, MCLEAP_VEC* displacement) {
	MCleap::map_cuda_buffers(m);

	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_vertices + block_size - 1) / dimBlock.x);

	move_mesh_kernel << <dimGrid, dimBlock >> > (m->num_vertices, m->d_vbo_v, velocity, dt, separation, cohesion, alignement, num_A, num_B, num_C, displacement);
}

void scale_mesh(MCleap::mcleap_mesh* m, float scale) {
	MCleap::map_cuda_buffers(m);

	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_vertices + block_size - 1) / dimBlock.x);

	scale_mesh_kernel << <dimGrid, dimBlock >> > (m->num_vertices, m->d_vbo_v, scale);
}

template<typename Functor, int queueSize, int visitedSizePerVertex>
void calcForces(Functor f, MCleap::mcleap_mesh* m, MCLEAP_VEC* d_vel, int* d_visited) {
	MCleap::map_cuda_buffers(m);
	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_vertices + block_size - 1) / dimBlock.x);

	//cudaMemset(d_visited, 0, sizeof(int) * m->num_vertices * visitedSizePerVertex);
	//cudaDeviceSynchronize();
	calcFRNN_frontier<Functor, queueSize, visitedSizePerVertex> <<<dimGrid, dimBlock >>> (f, m->d_vbo_v, m->d_t, m->d_mesh->he, m->d_mesh->v_to_he, d_visited, m->num_vertices, 50.0);
	//CudaCheckError();
	//cudaDeviceSynchronize();
}

int main(int argc, const char* argv[]) {
#ifdef BUILD_MCLEAP_WITH_OPENGL
	std::cout << "[Info] GLFW initialisation" << std::endl;

	// initialize the GLFW library
	if (!glfwInit()) {
		throw std::runtime_error("Couldn't init GLFW");
	}

	// setting the opengl version
	int major = 4;
	int minor = 2;
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// create the window
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Example 1", NULL, NULL);
	if (!window) {
		glfwTerminate();
		throw std::runtime_error("Couldn't create a window");
	}

	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();

	if (err != GLEW_OK) {
		glfwTerminate();
		throw std::runtime_error(std::string("Could initialize GLEW, error = ") +
			(const char*)glewGetErrorString(err));
	}

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER);
	const GLubyte* version = glGetString(GL_VERSION);
	std::cout << "Renderer: " << renderer << std::endl;
	std::cout << "OpenGL version supported " << version << std::endl;

	glfwMakeContextCurrent(window);

	Shader vsh(std::string("../resources/show_mesh.vs"), GL_VERTEX_SHADER);
	Shader fsh(std::string("../resources/show_mesh.fs"), GL_FRAGMENT_SHADER);
	ShaderProgram sh({ vsh, fsh });
	sh.use();
#endif

	std::string mesh(INITIAL_SIM_MESH);
	MCleap::mcleap_mesh* m = MCleap::load_off_mesh(mesh);
	if (m == nullptr) {
		printf("Coudn't find the file \"%s\"\n",mesh.c_str());
		return 0;
	}
#ifdef BUILD_MCLEAP_WITH_OPENGL
	GLuint vao = MCleap::get_vao(m);
#endif
	scale_mesh(m, SIM_SCALING);
	//scale_mesh(m, 0.1);

	MCLEAP_VEC* h_vel = new MCLEAP_VEC[m->num_vertices], * d_vel;
	for (int i = 4; i < m->num_vertices; i++) {
		h_vel[i].x = (i % 2 ? 1 : -1) * ((MCLEAP_REAL)i / m->num_vertices) * MAX_VEL;
		h_vel[i].y = (i % 2 ? -1 : 1) * ((MCLEAP_REAL)i / m->num_vertices) * MAX_VEL;
	}
	cudaMalloc((void**)&d_vel, m->num_vertices * sizeof(MCLEAP_VEC));
	cudaMemcpy(d_vel, h_vel, m->num_vertices * sizeof(MCLEAP_VEC), cudaMemcpyHostToDevice);

	MCleap::delaunay_transformation(m);

	int* d_visited;
	constexpr int queueSize = 40;
	constexpr int visitedPerVertex = 40;

	cudaMalloc((void**)&d_visited, sizeof(int) * m->num_vertices * visitedPerVertex);

	int numVertices = m->num_vertices;
	MCLEAP_VEC* separation, * cohesion, * alignement;
	int* num_A, * num_B, * num_C;
	MCLEAP_VEC* d_displacement;
	cudaMalloc((void**)&separation, numVertices * sizeof(MCLEAP_VEC));
	cudaMalloc((void**)&cohesion, numVertices * sizeof(MCLEAP_VEC));
	cudaMalloc((void**)&alignement, numVertices * sizeof(MCLEAP_VEC));
	cudaMalloc((void**)&num_A, numVertices * sizeof(int));
	cudaMalloc((void**)&num_B, numVertices * sizeof(int));
	cudaMalloc((void**)&num_C, numVertices * sizeof(int));
	cudaMalloc((void**)&d_displacement, numVertices * sizeof(MCLEAP_REAL) * 2);

	calcForces_functor f(m->num_vertices, d_vel, separation, cohesion, alignement, num_A, num_B, num_C);

	printf("InitialMesh information: NumEdges: %d NumVertices: %d\n", m->num_edges, m->num_vertices);

	int count = -1;
	std::string move("log/move0000.off");
	std::string dela("log/dela0000.off");
	std::string fixt("log/fixt0000.off");
	int stind = 8;

	MCleap::mcleap_mesh* m_copy = MCleap::init_empty_mesh(m->num_vertices, m->num_edges, m->num_triangles);

	std::string filename = "test.txt";
	std::ofstream s;
	s.open(filename);

#ifdef BUILD_MCLEAP_WITH_OPENGL
	while (!glfwWindowShouldClose(window)) {
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			std::string logfile("logmesh.off");
			MCleap::save_off_mesh(m, logfile);
		}
#else
	int simcounter = 100000;
	while(simcounter--){
#endif
		count++;
		MCleap::map_cuda_buffers(m);
		{
			MessageTimer sim("[Calc BFS]");
			//MessageTimer sim("[Update]");
			f.resetForces();
			calcForces<calcForces_functor, queueSize, visitedPerVertex>(f, m, d_vel, d_visited);
			//avoid_interpenetration(m, d_vel);
			move_mesh(m, d_vel, 1.0 / 60.0, separation, cohesion, alignement, num_A, num_B, num_C, d_displacement);
		}

		{
			MessageTimer tim("[Move mesh]");
			MCleap::move_vertices(m, m_copy, (MCLEAP_REAL*)d_displacement);
			MCleap::unmap_cuda_buffers(m);
		}

#ifdef BUILD_MCLEAP_WITH_OPENGL
		glBindVertexArray(vao);
		sh.use();
		sh.setUniform("u_zoom", 1.0 / (150.0 * SIM_SCALING));
		int display_w, display_h;
		glfwGetFramebufferSize(window, &display_w, &display_h);
		glViewport(0, 0, display_w, display_h);
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);


		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawElements(GL_TRIANGLES, m->num_triangles * 3, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(window);
		glfwPollEvents();
#endif
	}

#ifdef BUILD_MCLEAP_WITH_OPENGL
	glfwTerminate();
#endif
	delete m;
	return 0;
}