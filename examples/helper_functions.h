#pragma once

#include <chrono>
#include <fstream>
#include <iostream>

struct Timer {
	Timer() { begin = std::chrono::high_resolution_clock::now(); }
	std::chrono::high_resolution_clock::time_point begin, end;
};

struct MessageTimer : public Timer {
	MessageTimer(const char* message = "") : message(message) { begin = std::chrono::high_resolution_clock::now(); }
	~MessageTimer() { end = std::chrono::high_resolution_clock::now(); auto diff = std::chrono::duration<float, std::milli>(end - begin);  printf("%s%f milli\n", message, diff.count()); }
	const char* message;
	std::chrono::high_resolution_clock::time_point begin, end;
};

struct ofstreamTimer : public Timer {
	ofstreamTimer(std::ofstream& ofstream, const char* message = "") : ofstream(ofstream), message(message) { begin = std::chrono::high_resolution_clock::now(); }
	~ofstreamTimer() {
		end = std::chrono::high_resolution_clock::now();
		auto diff = std::chrono::duration_cast<std::chrono::microseconds>(end - begin);
		ofstream << message << " " << diff.count() << "\n";
	}
	std::ofstream& ofstream;
	std::string message;
	std::chrono::high_resolution_clock::time_point begin, end;
};

template<int i>
struct StaticCounter {
	static long long int count;
	static long long int getCount() {
		return count;
	}
	static void addOne() {
		count++;
	}
};

#define CREATE_STATIC_COUNTER(i) long long int StaticCounter<i>::count = 0;

#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

void printProgress(double percentage) {
	int val = (int)(percentage * 100);
	int lpad = (int)(percentage * PBWIDTH);
	int rpad = PBWIDTH - lpad;
	printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
	fflush(stdout);
}