#pragma once


#define CUDA_ERROR_CHECK
#ifdef CUDA_ERROR_CHECK
#define CudaCheckError()    __cudaCheckError( __FILE__, __LINE__ )
#else
#define CudaCheckError()
#endif

inline void __cudaCheckError(const char* file, const int line)
{
#ifdef CUDA_ERROR_CHECK
	cudaError err = cudaGetLastError();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "cudaCheckError() failed at %s:%i : %s\n",
			file, line, cudaGetErrorString(err));
		exit(-1);
	}

	// More careful checking. However, this will affect performance.
	// Comment away if needed.
	err = cudaDeviceSynchronize();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
			file, line, cudaGetErrorString(err));
		exit(-1);
	}
#endif

	return;
}

__device__ float kernel_walls(float x) {
	return -5 * exp(-0.01 * x * x + 4);
}

__device__ float clamp(float a, float b, float c) {
	if (a < b)return b;
	if (a > c)return c;
	return a;
}
