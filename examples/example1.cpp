#ifdef BUILD_MCLEAP_WITH_OPENGL
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Shader.hpp"
#include "glError.hpp"
#endif

#include <iostream>
#include <string>
#include <MCleap.h>

#include "helper_functions.h"

#define WIDTH 900
#define HEIGHT 900

int main(int argc, const char* argv[]) {

#ifdef BUILD_MCLEAP_WITH_OPENGL
	std::cout << "[Info] GLFW initialisation" << std::endl;

	// initialize the GLFW library
	if (!glfwInit()) {
		throw std::runtime_error("Couldn't init GLFW");
	}

	// setting the opengl version
	int major = 4;
	int minor = 0;
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// create the window
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Example 1", NULL, NULL);
	if (!window) {
		glfwTerminate();
		throw std::runtime_error("Couldn't create a window");
	}

	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();

	if (err != GLEW_OK) {
		glfwTerminate();
		throw std::runtime_error(std::string("Could initialize GLEW, error = ") +
			(const char*)glewGetErrorString(err));
	}

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER);
	const GLubyte* version = glGetString(GL_VERSION);
	std::cout << "Renderer: " << renderer << std::endl;
	std::cout << "OpenGL version supported " << version << std::endl;

	glfwMakeContextCurrent(window);

	Shader vsh(std::string("../resources/show_mesh.vs"), GL_VERTEX_SHADER);
	Shader fsh(std::string("../resources/show_mesh.fs"), GL_FRAGMENT_SHADER);
	ShaderProgram sh({ vsh, fsh });
	sh.use();

#endif // BUILD_MCLEAP_WITH_OPENGL
	
	std::string mesh("../resources/meshsmol.off");
	MCleap::mcleap_mesh* m = MCleap::load_off_mesh(mesh);

#ifdef BUILD_MCLEAP_WITH_OPENGL
	GLuint vao = MCleap::get_vao(m);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
#endif

	{
		MessageTimer time("[Transform delaunay] ");
		std::cout << (MCleap::delaunay_transformation(m) == 1 ? "SUCCESS" : "NO SUCCESS") << std::endl;
		MCleap::unmap_cuda_buffers(m);
	}
	
#ifdef BUILD_MCLEAP_WITH_OPENGL
	while (!glfwWindowShouldClose(window)) {

		int display_w, display_h;
		glfwGetFramebufferSize(window, &display_w, &display_h);
		glViewport(0, 0, display_w, display_h);

		glClear(GL_COLOR_BUFFER_BIT);

		sh.use();
		sh.setUniform(std::string("u_zoom"), 1.0 / 150.0);

		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, MCleap::triangle_count(m) * 3, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
#endif // BUILD_MCLEAP_WITH_OPENGL

	delete m;

	return 0;
}
