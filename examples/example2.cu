#ifdef BUILD_MCLEAP_WITH_OPENGL
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Shader.hpp"
#include "glError.hpp"
#endif

#include <iostream>
#include <string>
#include <mcleap_mesh.cuh>
#include <MCleap.h>

#include <random>

#include "helper_functions.h"

#define WIDTH 900
#define HEIGHT 900

#define VELOCITY 0.05
#define RADIUS 0.5

#define INITIAL_SIM_MESH "../resources/mesh10000.off"
#define ITERNAME(text,num) "text##num"

using MCleap::MCLEAP_REAL;
using MCleap::MCLEAP_VEC;

__global__ void move_mesh_kernel(int numVertices, MCLEAP_VEC* vertexData, MCLEAP_REAL* velocity, MCLEAP_REAL* displacement) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < numVertices && i>3) {
		if (abs(vertexData[i].x + velocity[i * 2 + 0]) > 100)
			velocity[i * 2 + 0] *= -1;
		if (abs(vertexData[i].y + velocity[i * 2 + 1]) > 100)
			velocity[i * 2 + 1] *= -1;
		displacement[i*2+0] = velocity[i * 2 + 0];
		displacement[i*2+1] = velocity[i * 2 + 1];
	}
}

__device__ float dist(MCLEAP_VEC v0, MCLEAP_VEC v1) {
	return sqrt((v0.x - v1.x) * (v0.x - v1.x) + (v0.y - v1.y) * (v0.y - v1.y));
}

__global__ void avoid_interpenetration_kernel(int numVertices, MCLEAP_VEC* vertexData, MCLEAP_REAL* velocity, int numEdges, MCleap::edge* hed) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < numEdges && (hed[i].tb.x != -1)) {
		int id0 = hed[i].v.x;
		int id1 = hed[i].v.y;
		MCLEAP_VEC v0 = vertexData[id0] + MCLEAP_VEC{ velocity[id0 * 2 + 0],velocity[id0 * 2 + 1] };
		MCLEAP_VEC v1 = vertexData[id1] + MCLEAP_VEC{ velocity[id1 * 2 + 0],velocity[id1 * 2 + 1] };

		if (dist(v0, v1) < RADIUS) {
			velocity[id0 * 2 + 0] *= -1;
			velocity[id0 * 2 + 1] *= -1;
			velocity[id1 * 2 + 0] *= -1;
			velocity[id1 * 2 + 1] *= -1;
		}
	}
}

void avoid_interpenetration(MCleap::mcleap_mesh* m, MCLEAP_REAL* velocity) {
	MCleap::map_cuda_buffers(m);

	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_edges + block_size - 1) / dimBlock.x);

	avoid_interpenetration_kernel << <dimGrid, dimBlock >> > (m->num_vertices, m->d_vbo_v, velocity, m->num_edges, m->d_mesh->he);
}

void move_mesh(MCleap::mcleap_mesh* m, MCLEAP_REAL* velocity, MCLEAP_REAL* displacement) {
	MCleap::map_cuda_buffers(m);

	int block_size = 256;
	dim3 dimBlock(block_size);
	dim3 dimGrid((m->num_vertices + block_size - 1) / dimBlock.x);

	move_mesh_kernel << <dimGrid, dimBlock >> > (m->num_vertices, m->d_vbo_v, velocity, displacement);
}

int main(int argc, const char* argv[]) {
#ifdef BUILD_MCLEAP_WITH_OPENGL
	std::cout << "[Info] GLFW initialisation" << std::endl;

	// initialize the GLFW library
	if (!glfwInit()) {
		throw std::runtime_error("Couldn't init GLFW");
	}

	// setting the opengl version
	int major = 4;
	int minor = 2;
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// create the window
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Example 1", NULL, NULL);
	if (!window) {
		glfwTerminate();
		throw std::runtime_error("Couldn't create a window");
	}

	glfwMakeContextCurrent(window);

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();

	if (err != GLEW_OK) {
		glfwTerminate();
		throw std::runtime_error(std::string("Could initialize GLEW, error = ") +
			(const char*)glewGetErrorString(err));
	}

	// get version info
	const GLubyte* renderer = glGetString(GL_RENDERER);
	const GLubyte* version = glGetString(GL_VERSION);
	std::cout << "Renderer: " << renderer << std::endl;
	std::cout << "OpenGL version supported " << version << std::endl;

	glfwMakeContextCurrent(window);

	Shader vsh(std::string("../resources/show_mesh.vs"), GL_VERTEX_SHADER);
	Shader fsh(std::string("../resources/show_mesh.fs"), GL_FRAGMENT_SHADER);
	ShaderProgram sh({ vsh, fsh });
	sh.use();
#endif

	std::string mesh(INITIAL_SIM_MESH);
	MCleap::mcleap_mesh* m = MCleap::load_off_mesh(mesh);
#ifdef BUILD_MCLEAP_WITH_OPENGL
	GLuint vao = MCleap::get_vao(m);
#endif
	MCleap::delaunay_transformation(m);

	MCLEAP_REAL* h_vel = new MCLEAP_REAL[m->num_vertices * 2], * d_vel, *d_displ;
	for (int i = 0; i < m->num_vertices; i++) {
		h_vel[i * 2 + 0] = VELOCITY;
		h_vel[i * 2 + 1] = -VELOCITY;
	}
	cudaMalloc((void**)&d_vel, m->num_vertices * sizeof(MCLEAP_REAL) * 2);
	cudaMalloc((void**)&d_displ, m->num_vertices * sizeof(MCLEAP_REAL) * 2);
	cudaMemcpy(d_vel, h_vel, m->num_vertices * sizeof(MCLEAP_REAL) * 2, cudaMemcpyHostToDevice);

	MCleap::mcleap_mesh* m_copy = MCleap::init_empty_mesh(m->num_vertices, m->num_edges, m->num_triangles);

	std::string filename = "test.txt";
	std::ofstream s;
	s.open(filename);

#ifdef BUILD_MCLEAP_WITH_OPENGL
	while (!glfwWindowShouldClose(window)) {
#else 
	int simcounter = 1000;
	while(simcounter--) {
#endif
		avoid_interpenetration(m, d_vel);
		move_mesh(m, d_vel, d_displ);
		{
			ofstreamTimer time(s);

			//MessageTimer time("[Move particles] ");
			MCleap::move_vertices(m, m_copy, d_displ);
			MCleap::unmap_cuda_buffers(m);
		}
#ifdef BUILD_MCLEAP_WITH_OPENGL
		glBindVertexArray(vao);
		sh.use();
		const std::string zoom("u_zoom");
		sh.setUniform(zoom, 1.0 / 150.0);
		int display_w, display_h;
		glfwGetFramebufferSize(window, &display_w, &display_h);
		glViewport(0, 0, display_w, display_h);
		glClearColor(0.0f, 0.01f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawElements(GL_TRIANGLES, m->num_triangles * 3, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(window);
		glfwPollEvents();
#endif
	}
	s.close();

#ifdef BUILD_MCLEAP_WITH_OPENGL
	glfwTerminate();
#endif
	delete m;

	return 0;
}
