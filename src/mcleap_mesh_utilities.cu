#include "helper.cuh"
#include "../include/mcleap_mesh.cuh"

#include <map>
#include <fstream>

namespace MCleap {

	RESULT_CODE map_cuda_buffers(mcleap_mesh* m) {
		if (m->h_buffers_cuda_mapped)return FAILURE;
#ifdef BUILD_MCLEAP_WITH_OPENGL
		cudaGraphicsMapResources(1, &m->d_mesh->vbo_v_cuda, 0);
		cudaGraphicsMapResources(1, &m->d_mesh->ebo_t_cuda, 0);
		cudaGraphicsResourceGetMappedPointer((void**)&m->d_vbo_v, &m->bytes, m->d_mesh->vbo_v_cuda);
		cudaGraphicsResourceGetMappedPointer((void**)&m->d_t, &m->bytes, m->d_mesh->ebo_t_cuda);
#endif
		m->h_buffers_cuda_mapped = true;
		return SUCCESS;
	}

	RESULT_CODE unmap_cuda_buffers(mcleap_mesh* m) {
		if (!m->h_buffers_cuda_mapped)return FAILURE;
#ifdef BUILD_MCLEAP_WITH_OPENGL
		cudaGraphicsUnmapResources(1, &m->d_mesh->vbo_v_cuda, 0);
		cudaGraphicsUnmapResources(1, &m->d_mesh->ebo_t_cuda, 0);
#endif
		m->h_buffers_cuda_mapped = false;
		return SUCCESS;
	}

	int vertex_count(mcleap_mesh* m) {
		if (m == nullptr)
			return 0;
		return m->num_vertices;
	}

	int edge_count(mcleap_mesh* m) {
		if (m == nullptr)
			return 0;
		return m->num_edges;
	}

	int triangle_count(mcleap_mesh* m) {
		if (m == nullptr)
			return 0;
		return m->num_triangles;
	}

	mcleap_mesh* load_triangle_soup(std::string& filename) {
		std::ifstream file(filename.c_str(), std::ifstream::in);
		if (!file.is_open()) {
			printf("MCLEAP::FILE \" %s \" NOT FOUND", filename.c_str());
			return nullptr;
		}
		std::string::size_type idx;
		idx = filename.rfind('.');
		if (idx != std::string::npos) {
			std::string extension = filename.substr(idx + 1);
			if (extension != "off") {
				printf("MCLEAP::FILE \" %s \" HAS A WRONG EXTENSION", filename.c_str());
				file.close();
				return nullptr;
			}
		}

		mcleap_mesh* m = new mcleap_mesh();

		std::string line;
		std::getline(file, line);  // OFF line
		while (true) {
			std::getline(file, line);
			if (line[0] != '#')
				break;
		}
		sscanf(line.c_str(), "%d %d %d\n", &m->num_vertices, &m->num_triangles, &m->num_edges);
		m->v = new MCLEAP_REAL[m->num_vertices * 2];
		m->v_to_he = new uint[m->num_vertices];
		m->t = new uint[3 * m->num_triangles];
		m->t_to_he = new uint[m->num_triangles];

		for (int i = 0; i < m->num_vertices; i++) {
			std::getline(file, line);
			MCLEAP_REAL dummy = 0.0;
#ifdef MCLEAP_DOUBLE_PRECISION
			sscanf(line.c_str(), "%lf %lf %lf\n", &m->v[i * 2 + 0], &m->v[i * 2 + 1], &dummy);
#else
			sscanf(line.c_str(), "%f %f %f\n", &m->v[i * 2 + 0], &m->v[i * 2 + 1], &dummy);
#endif
		}
		int num;
		for (int i = 0; i < m->num_triangles; i++) {
			std::getline(file, line);
			sscanf(line.c_str(), "%d %d %d %d\n", &num, &m->t[i * 3 + 0], &m->t[i * 3 + 1], &m->t[i * 3 + 2]);
			if (num != 3) {
				delete m;
				printf("MCLEAP::INPUT MESH:: MCLEAP ONLY WORKS WITH TRIANGLE MESHES");
				file.close();
				return nullptr;
			}
		}
		file.close();
		return m;
	}

	void build_from_soup(mcleap_mesh* m) {
		if (m == nullptr)
			return;
		// Fix non CCW triangles, just in case ...
		// It uses CPU adaptive Shewchuk's geometric predicates
		for (int i = 0; i < m->num_triangles; i++) {
			fix_ccw_2d_host(m, i);
		}

		// Key -> vertices, Value -> triangle index
		std::map<pair, int> vertices_to_triangles;
		int edgeCount = 0;

		for (int i = 0; i < m->num_triangles; i++) {
			for (int j = 0; j < 3; j++) {
				int i1 = m->t[i * 3 + j];
				int i2 = m->t[i * 3 + (j + 1) % 3];
				pair p(i1, i2);
				pair rp(i2, i1);
				if (vertices_to_triangles.find(rp) == vertices_to_triangles.end())
					edgeCount++;
				vertices_to_triangles[p] = i;
			}

		}

		m->num_edges = edgeCount;
		m->he = new edge[m->num_edges];

		// Key -> vertices, Value -> Half Edge index (Edge Index * 2 + i, with 0 <= i <= 1)
		// The half edge index is ind * 2 when is the first twin, and it's ind*2+1 when it's the second twin
		std::map<pair, int> vertices_to_edges;

		unsigned int count = 0;
		for (auto pet : vertices_to_triangles) {
			int t = pet.second;
			pair v = pet.first;
			pair inv = pair(GET1(v), GET0(v));

			int ind = -1;

			if (vertices_to_edges.find(inv) == vertices_to_edges.end()) {
				ind = count;
				for (int i = 0; i < 3; i++) {
					if (m->t[t * 3 + i] == GET0(v))
						m->he[ind].ta.x = t * 3 + i;
					else if (m->t[t * 3 + i] == GET1(v))
						m->he[ind].ta.y = t * 3 + i;
					else
						m->he[ind].op.x = t * 3 + i;
				}
				m->he[ind].v.x = GET0(v);
				m->he[ind].v.y = GET1(v);

				vertices_to_edges[v] = ind * 2;

				m->t_to_he[t] = ind * 2;
				m->v_to_he[GET0(v)] = ind * 2;
				m->v_to_he[GET1(v)] = ind * 2 + 1;
				count++;
			}
			else {
				ind = vertices_to_edges[inv] / 2;
				for (int i = 0; i < 3; i++) {
					if (m->t[t * 3 + i] == GET0(v))
						m->he[ind].tb.x = t * 3 + i;
					else if (m->t[t * 3 + i] == GET1(v))
						m->he[ind].tb.y = t * 3 + i;
					else
						m->he[ind].op.y = t * 3 + i;
				}
				vertices_to_edges[v] = ind * 2 + 1;

				m->t_to_he[t] = ind * 2 + 1;
				m->v_to_he[GET0(v)] = ind * 2 + 1;
				m->v_to_he[GET1(v)] = ind * 2 + 0;
			}
		}

		// populating m->he[i].e
		for (int i = 0; i < m->num_edges; i++) {
			int ta1 = m->he[i].ta.y;
			int i1 = ta1 / 3;
			int j1 = ta1 % 3;
			int ta2 = 3 * i1 + (j1 + 1) % 3;
			int nEdgeA = vertices_to_edges[pair(m->t[ta1], m->t[ta2])];

			int tb1 = m->he[i].tb.y;
			int i2 = tb1 / 3;
			int j2 = tb1 % 3;
			int tb2 = 3 * i2 + (j2 + 1) % 3;
			int nEdgeB = vertices_to_edges[pair(m->t[tb1], m->t[tb2])];
			if (tb1 == -1)
				nEdgeB = -1;

			m->he[i].e.x = nEdgeA;
			m->he[i].e.y = nEdgeB;

		}
	}

	void transfer_to_host(mcleap_mesh* m) {
		if (m == nullptr)
			return;

		cudaMemcpy(m->v_to_he, m->d_mesh->v_to_he, m->num_vertices * sizeof(int), cudaMemcpyDeviceToHost);
		cudaMemcpy(m->t_to_he, m->d_mesh->t_to_he, m->num_triangles * sizeof(int), cudaMemcpyDeviceToHost);
		cudaMemcpy(m->he, m->d_mesh->he, m->num_edges * sizeof(edge), cudaMemcpyDeviceToHost);

		map_cuda_buffers(m);

		cudaMemcpy(m->v, m->d_vbo_v, m->num_vertices * sizeof(MCLEAP_REAL) * 2, cudaMemcpyDeviceToHost);
		cudaMemcpy(m->he, m->d_mesh->he, m->num_edges * sizeof(edge), cudaMemcpyDeviceToHost);
		cudaMemcpy(m->t, m->d_t, m->num_triangles * sizeof(uint) * 3, cudaMemcpyDeviceToHost);
	}

	void print_mesh(mcleap_mesh* m) {
		if (m == nullptr)
			return;

		transfer_to_host(m);

		for (int i = 0; i < m->num_vertices; i++) {
			std::cout << "Vertex " << i << ": " << m->v[i * 2 + 0] << " "
				<< m->v[i * 2 + 1] << std::endl;
		}
		for (int i = 0; i < m->num_triangles; i++) {
			std::cout << "Triangle " << i << ": " << m->t[i * 3 + 0] << " "
				<< m->t[i * 3 + 1] << " " << m->t[i * 3 + 2] << " " << std::endl;
		}
		for (int i = 0; i < m->num_edges; i++) {
			std::cout << i << " " << m->he[i] << std::endl;
		}
		std::cout << "v_to_he: ";
		for (int i = 0; i < m->num_vertices; i++) {
			std::cout << m->v_to_he[i] << " ";
		}std::cout << std::endl;

	}

	// TODO: divide this method in 2: One does the transfer and the other allocates memory
	void transfer_to_device(mcleap_mesh* m) {
		if (m == nullptr)
			return;
		if (m->d_mesh != nullptr)
			delete m->d_mesh;

		m->d_mesh = new mcleap_device_mesh();
		auto d_mesh = m->d_mesh;
		cudaMalloc((void**)&d_mesh->v_to_he, m->num_vertices * sizeof(int));
		cudaMalloc((void**)&d_mesh->t_to_he, m->num_triangles * sizeof(int));
		cudaMalloc((void**)&d_mesh->he, m->num_edges * sizeof(edge));

		cudaMalloc((void**)&d_mesh->d_listo, 2 * sizeof(int));

		cudaMemcpy(d_mesh->v_to_he, m->v_to_he, m->num_vertices * sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(d_mesh->t_to_he, m->t_to_he, m->num_triangles * sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(d_mesh->he, m->he, m->num_edges * sizeof(edge), cudaMemcpyHostToDevice);

		cudaMalloc((void**)&d_mesh->trirel, m->num_triangles * sizeof(int));
		cudaMalloc((void**)&d_mesh->trireservs, m->num_triangles * sizeof(int));

#ifdef BUILD_MCLEAP_WITH_OPENGL
		cudaError_t err;

		glGenVertexArrays(1, &d_mesh->vao);
		glGenBuffers(1, &d_mesh->vbo_v);
		glGenBuffers(1, &d_mesh->ebo_t);

		glBindVertexArray(d_mesh->vao);

		glBindBuffer(GL_ARRAY_BUFFER, d_mesh->vbo_v);
		glBufferData(GL_ARRAY_BUFFER, m->num_vertices * 2 * sizeof(MCLEAP_REAL), m->v, GL_STREAM_DRAW);

		err = cudaGraphicsGLRegisterBuffer(&d_mesh->vbo_v_cuda, d_mesh->vbo_v, cudaGraphicsMapFlagsNone);
		if (err != cudaSuccess)
			printf("MCLEAP::device_load_mesh::cudaGraphicsRegisterBuffer::vbo_v::%s\n", cudaGetErrorString(err));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, d_mesh->ebo_t);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m->num_triangles * 3 * sizeof(GLuint), m->t, GL_STREAM_DRAW);

	#ifdef MCLEAP_DOUBLE_PRECISION
		glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 0, 0);
	#else // NO MCLEAP_DOUBLE_PRECISION
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	#endif // MCLEAP_DOUBLE_PRECISION
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		err = cudaGraphicsGLRegisterBuffer(&d_mesh->ebo_t_cuda, d_mesh->ebo_t, cudaGraphicsMapFlagsNone);
		if (err != cudaSuccess)
			printf("MCLEAP::device_load_mesh::cudaGraphicsRegisterBuffer::ebo_t::%s\n", cudaGetErrorString(err));
		glBindVertexArray(0);
#else // NO BUILD_MCLEAP_WITH_OPENGL
		cudaMalloc((void**)&m->d_vbo_v, m->num_vertices * sizeof(MCLEAP_VEC));
		cudaMalloc((void**)&m->d_t, m->num_triangles * 3 * sizeof(uint));

		cudaMemcpy(m->d_vbo_v, m->v, m->num_vertices * sizeof(MCLEAP_VEC), cudaMemcpyHostToDevice);
		cudaMemcpy(m->d_t, m->t, m->num_triangles * 3 * sizeof(uint), cudaMemcpyHostToDevice);

#endif // BUILD_MCLEAP_WITH_OPENGL
	}

	mcleap_mesh* load_off_mesh(std::string& filename) {
		if (!initialized_cuda) {
			//cudaGLSetGLDevice(0);
			initialized_cuda = true;
		}
		mcleap_mesh* m = load_triangle_soup(filename); // gets the triangle soup information
		if (m == nullptr)
			return m;
		build_from_soup(m);  // gets the halfedge information given a triangle soup
		transfer_to_device(m); // transfers the mesh data to the gpu

		return m;
	}

	RESULT_CODE save_off_mesh(mcleap_mesh* m, std::string& filename) {
		if (m == nullptr)
			return FAILURE;
		transfer_to_host(m);
		std::fstream file_out;
		file_out.open(filename, std::ios_base::out);
		if (!file_out.is_open()) {
			LOG("[save_off_mesh] Failed to write file %s\n", filename.c_str());
			return FAILURE;
		}
		file_out << "OFF\n";
		file_out << vertex_count(m) << " " << triangle_count(m) << " " << edge_count(m) << "\n";
		for (int i = 0; i < vertex_count(m); i++) {
			file_out << m->v[i * 2 + 0] << " " << m->v[i * 2 + 1] << " 0.0\n";
		}
		for (int i = 0; i < triangle_count(m); i++) {
			file_out << "3 " << m->t[i * 3 + 0] << " " << m->t[i * 3 + 1] << " " << m->t[i * 3 + 2] << "\n";
		}
		file_out.close();
		LOG("[save_off_mesh] file %s written.\n", filename.c_str());
		return SUCCESS;
	}

#ifdef BUILD_MCLEAP_WITH_OPENGL
	GLuint get_vertex_vbo(mcleap_mesh* m) {
		if (m == nullptr)
			return 0;
		return m->d_mesh->vbo_v;
	}

	GLuint get_triangles_ebo(mcleap_mesh* m) {
		if (m == nullptr)
			return 0;
		return m->d_mesh->ebo_t;
	}

	GLuint get_vao(mcleap_mesh* m) {
		if (m == nullptr)
			return 0;
		return m->d_mesh->vao;
	}

	GLuint* get_host_indices(mcleap_mesh* m) {
		if (m == nullptr)
			return 0;
		return m->t;
	}
#endif // BUILD_MCLEAP_WITH_OPENGL

	mcleap_mesh* init_empty_mesh(int num_vertices, int num_edges, int num_triangles) {
		mcleap_mesh* m = new mcleap_mesh();
		m->num_edges = num_edges;
		m->num_vertices = num_vertices;
		m->num_triangles = num_triangles;

		m->v = new MCLEAP_REAL[m->num_vertices * 2];
		m->v_to_he = new uint[m->num_vertices];
		m->t = new uint[3 * m->num_triangles];
		m->t_to_he = new uint[m->num_triangles];
		m->he = new edge[m->num_edges];

		transfer_to_device(m);
		return m;
	}

	RESULT_CODE deep_device_mesh_copy(mcleap_mesh* m_dest, mcleap_mesh* m_origin) {

		cudaMemcpy(m_dest->d_mesh->v_to_he, m_origin->d_mesh->v_to_he, m_origin->num_vertices * sizeof(int), cudaMemcpyDeviceToDevice);
		cudaMemcpy(m_dest->d_mesh->t_to_he, m_origin->d_mesh->t_to_he, m_origin->num_triangles * sizeof(int), cudaMemcpyDeviceToDevice);
		cudaMemcpy(m_dest->d_mesh->he, m_origin->d_mesh->he, m_origin->num_edges * sizeof(edge), cudaMemcpyDeviceToDevice);

		map_cuda_buffers(m_dest);
		map_cuda_buffers(m_origin);

		cudaMemcpy(m_dest->d_vbo_v, m_origin->d_vbo_v, m_origin->num_vertices * 2 * sizeof(MCLEAP_REAL), cudaMemcpyDeviceToDevice);
		cudaMemcpy(m_dest->d_t, m_origin->d_t, m_origin->num_triangles * 3 * sizeof(uint), cudaMemcpyDeviceToDevice);

		return SUCCESS;
	}
};