#pragma once

#include "../include/mcleap_mesh.cuh"

#include <tuple>
#include <iostream>

namespace MCleap {

	/// -------------------- Logging --------------------------- //
//#define MCLEAP_LOGGING
#ifdef MCLEAP_LOGGING
#define LOG(...) printf(__VA_ARGS__)
#else
#define LOG(...)
#endif

	void fix_ccw_2d_host(mcleap_mesh* m, int index);

	typedef std::tuple<int, int> pair;
	typedef std::tuple<int, int, int> triplet;

#define GET(I, X) std::get<I>(X)
#define GET0(X) GET(0, X)
#define GET1(X) GET(1, X)
#define GET2(X) GET(2, X)

	std::ostream& operator<<(std::ostream& o, pair p);
	std::ostream& operator<<(std::ostream& o, edge e);

	template <class T>
	T min(T a, T b) {
		if (a < b)
			return a;
		return b;
	}
	template <class T>
	T max(T a, T b) {
		if (a > b)
			return a;
		return b;
	}

	template<int i>
	struct StaticCounter {
		static long long int count;
		static long long int getCount() {
			return count;
		}
		static void addOne() {
			count++;
		}
	};

	#define CREATE_STATIC_COUNTER(i) long long int StaticCounter<i>::count = 0;
};