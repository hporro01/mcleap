#include "helper.cuh"

#include "../lib/predicates.h"

namespace MCleap {

	static bool initilalized_predicates = false;
	void fix_ccw_2d_host(mcleap_mesh* m, int index) {
		if (!initilalized_predicates) {
			exactinit();
			initilalized_predicates = true;
		}
		unsigned int i[3] = { m->t[index * 3 + 0],m->t[index * 3 + 1],m->t[index * 3 + 2] };
		double v[6] = { m->v[i[0] * 2 + 0], m->v[i[0] * 2 + 1],
						m->v[i[1] * 2 + 0], m->v[i[1] * 2 + 1],
						m->v[i[2] * 2 + 0], m->v[i[2] * 2 + 1]
		};
		if (orient2d(v, v + 2, v + 4) < 0) {
			printf("%d\n", orient2d(v, v + 2, v + 4) > 0);
			int aux = m->t[index * 3 + 0];
			m->t[index * 3 + 0] = m->t[index * 3 + 1];
			m->t[index * 3 + 1] = aux;
		}
	}

	std::ostream& operator<<(std::ostream& o, pair p) {
		o << std::get<0>(p) << " " << std::get<1>(p);
		return o;
	}

	std::ostream& operator<<(std::ostream& o, edge e) {
		o << "Edge : ta: " << e.ta.x << " " << e.ta.y
			<< " tb: " << e.tb.x << " " << e.tb.y
			<< " op: " << e.op.x << " " << e.op.y
			<< " v: " << e.v.x << " " << e.v.y
			<< " next: " << e.e.x << " " << e.e.y;
		return o;
	}

};