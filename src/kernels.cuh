#pragma once

#include "../include/mcleap_mesh.cuh"
#include "helper.cuh"

namespace MCleap {

#define MCLEAP_TRIANGLE_ZERO_AREA_EPS 0.000001
#define MCLEAP_EPSILON_DELAUNAY 0.0000001f
#define MCLEAP_PI 3.14159265f

	inline __device__ __host__ MCLEAP_REAL cross(MCLEAP_VEC u, MCLEAP_VEC v) {
		return u.x * v.y - v.x * u.y;
	}

	inline __device__ __host__ MCLEAP_VEC distVec(MCLEAP_VEC a, MCLEAP_VEC b) {
		return MCLEAP_VEC{ b.x - a.x, b.y - a.y };
	}

	__global__ void init_device_dual_arrays_int(int* array1, int* array2, int size, int value) {
		unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
		if (i < size) {
			array1[i] = value;
			array2[i] = value;
		}
	}

	inline __device__ float mcleap_d_dot_product(MCLEAP_VEC u, MCLEAP_VEC v) {
		return u.x * v.x + u.y * v.y;
	}
	inline __device__ MCLEAP_REAL mcleap_d_cross_product(MCLEAP_VEC u, MCLEAP_VEC v) {
		return u.x * v.y - v.x * u.y;	
	}

	inline __device__ MCLEAP_REAL mcleap_d_angle_vectors(MCLEAP_VEC u, MCLEAP_VEC v) {
#ifdef MCLEAP_DOUBLE_PRECISION
		return atan2f(fabs(mcleap_d_cross_product(u, v)), mcleap_d_dot_product(u, v)); // TODO: Get the double version of this function. For now, the compiler does not find the function atan2
#else
		return atan2f(fabsf(mcleap_d_cross_product(u, v)), mcleap_d_dot_product(u, v));
#endif
	}

	inline __device__ int mcleap_d_delaunay_test_2d(const MCLEAP_VEC* mesh_data, const int op1, const int op2, const int com_a, const int com_b) {
		MCLEAP_VEC u; // vector
		MCLEAP_VEC p, q; // points
		// get two vectors of the first triangle
		p = mesh_data[op1];
		q = mesh_data[com_a];
		u = q-p; //! + 5 flop
		q = mesh_data[com_b];
		MCLEAP_REAL alpha = mcleap_d_angle_vectors(u, q-p);
		// the same for other triangle
		p = mesh_data[op2];
		q = mesh_data[com_a];
		u = q-p;
		q = mesh_data[com_b];
		MCLEAP_REAL beta = mcleap_d_angle_vectors(u, q-p);

#ifdef MCLEAP_DOUBLE_PRECISION
		return (int)(fabs(alpha + beta) / MCLEAP_PI - MCLEAP_EPSILON_DELAUNAY);
#else
		return (int)(fabsf(alpha + beta) / MCLEAP_PI - MCLEAP_EPSILON_DELAUNAY);
#endif
	}

	__device__ __host__ int invertedTriangleTest(MCLEAP_VEC op1, MCLEAP_VEC op2, MCLEAP_VEC e1, MCLEAP_VEC e2) {
		MCLEAP_VEC v0 = distVec(e1, e2);
		MCLEAP_VEC v2 = distVec(e1, op2);
		MCLEAP_VEC v1 = distVec(e1, op1);

		MCLEAP_REAL d = cross(v2, v0);
		MCLEAP_REAL s = cross(v1, v0);
		MCLEAP_REAL t = cross(v2, v1);

		// this is the case where two triangles get their area close to zero
		if ((abs(t) < MCLEAP_TRIANGLE_ZERO_AREA_EPS) && (abs(s) < MCLEAP_TRIANGLE_ZERO_AREA_EPS) && (abs(d) < MCLEAP_TRIANGLE_ZERO_AREA_EPS)) {
			printf("v0: %f %f, v1: %f %f, v2: %f %f, d:%f, s:%f, t:%f\n", v0.x, v0.y, v1.x, v1.y, v2.x, v2.y, d, s, t);
			return -1;
		}

		bool pred1 = ((d < 0 && s <= 0 && t <= 0) ||
			(d > 0 && s >= 0 && t >= 0) ||
			(s < 0 && d <= 0 && -t <= 0) ||
			(s > 0 && d >= 0 && -t >= 0));

		bool pred2 = pred1 && ((d < 0 && s <= 0 && t <= 0 && s + t >= d) ||
			(d > 0 && s >= 0 && t >= 0 && s + t <= d) ||
			(s < 0 && d <= 0 && -t <= 0 && d - t >= s) ||
			(s > 0 && d >= 0 && -t >= 0 && d - t <= s));

		if (pred1 && !pred2) {
			return -2;
		}

		return pred2;
	}

	inline __device__ void flip_edge(
		uint* triangles,
		edge* hed,
		int* trirel,
		int* trireservs,
		int* v_to_he,
		int2* a_shared_array,
		int2* b_shared_array,
		int2* op_shared_array) 
	{
		const int i = blockIdx.x * blockDim.x + threadIdx.x;
		trirel[a_shared_array[threadIdx.x].y / 3] = b_shared_array[threadIdx.x].y / 3; //! + 8 flop
		trirel[b_shared_array[threadIdx.x].y / 3] = a_shared_array[threadIdx.x].y / 3; //! + 8 flop
		// exchange necessary indexes
		triangles[a_shared_array[threadIdx.x].y] = triangles[op_shared_array[threadIdx.x].y];
		triangles[b_shared_array[threadIdx.x].y] = triangles[op_shared_array[threadIdx.x].x];
		// update the indices of the flipped edge.
		hed[i].ta = make_int2(a_shared_array[threadIdx.x].y, op_shared_array[threadIdx.x].x);
		hed[i].tb = make_int2(b_shared_array[threadIdx.x].y, op_shared_array[threadIdx.x].y);
		// update vertex indices
		hed[i].v = make_int2(triangles[op_shared_array[threadIdx.x].y], triangles[op_shared_array[threadIdx.x].x]);

		v_to_he[hed[i].v.x] = i * 2 + 0;
		v_to_he[hed[i].v.y] = i * 2 + 1;

		// update oppposites indices
		hed[i].op = make_int2(a_shared_array[threadIdx.x].x, b_shared_array[threadIdx.x].x);

		int2 next = hed[i].e;

		v_to_he[triangles[hed[i].op.x]] = next.y;
		v_to_he[triangles[hed[i].op.y]] = next.x;

		int2 next_next = make_int2(
			next.x % 2 ? hed[next.x / 2].e.y : hed[next.x / 2].e.x,
			next.y % 2 ? hed[next.y / 2].e.y : hed[next.y / 2].e.x);

		hed[i].e = next_next;

		next.x % 2 ? hed[next.x / 2].e.y = i * 2 + 1
			: hed[next.x / 2].e.x = i * 2 + 1;
		next.y % 2 ? hed[next.y / 2].e.y = i * 2 + 0
			: hed[next.y / 2].e.x = i * 2 + 0;

		next_next.x % 2 ? hed[next_next.x / 2].e.y = next.y
			: hed[next_next.x / 2].e.x = next.y;
		next_next.y % 2 ? hed[next_next.y / 2].e.y = next.x
			: hed[next_next.y / 2].e.x = next.x;
	}

	template<unsigned int block_size>
	__global__ void delaunay_kernel(MCLEAP_VEC* mesh_data, uint* triangles, edge* hed, int edge_count, int* listo, int* trirel, int* trireservs, int* v_to_he) {

		const int i = blockIdx.x * blockDim.x + threadIdx.x; //! + 2 flop
		__shared__ int2 a_shared_array[block_size];
		__shared__ int2 b_shared_array[block_size];
		__shared__ int2 op_shared_array[block_size];
		if (i < edge_count) {
			a_shared_array[threadIdx.x] = hed[i].ta;
			b_shared_array[threadIdx.x] = hed[i].tb;
			op_shared_array[threadIdx.x] = hed[i].op;

			if (b_shared_array[threadIdx.x].x != -1) {
				if (mcleap_d_delaunay_test_2d(mesh_data, triangles[op_shared_array[threadIdx.x].x], triangles[op_shared_array[threadIdx.x].y], triangles[a_shared_array[threadIdx.x].x], triangles[a_shared_array[threadIdx.x].y]) > 0) {
					//if( cleap_d_delaunay_test_2d_det( mesh_data, triangles[op_shared_array[threadIdx.x].x], triangles[op_shared_array[threadIdx.x].y], triangles[a_shared_array[threadIdx.x].x], triangles[a_shared_array[threadIdx.x].y]) > 0) {
					listo[0] = 0;
					// exclusion part
					if (atomicExch(&(trireservs[a_shared_array[threadIdx.x].y / 3]), i) == -1 && atomicExch(&(trireservs[b_shared_array[threadIdx.x].y / 3]), i) == -1) { //!  + 8 flop
						// proceed to flip the edges
						LOG("[delaunay_kernel] Flip edge %d, triangles %d %d, t1 vertices %d %d %d, t2 vertices %d %d %d\n",
							i, a_shared_array[threadIdx.x].y / 3,
							b_shared_array[threadIdx.x].y / 3,
							triangles[a_shared_array[threadIdx.x].x],
							triangles[a_shared_array[threadIdx.x].y],
							triangles[op_shared_array[threadIdx.x].x],
							triangles[b_shared_array[threadIdx.x].x],
							triangles[b_shared_array[threadIdx.x].y],
							triangles[op_shared_array[threadIdx.x].y]);

						flip_edge(triangles,
							hed,
							trirel,
							trireservs,
							v_to_he,
							a_shared_array,
							b_shared_array,
							op_shared_array);

					}
				}
			}
		}
	}

	__global__ void mcleap_kernel_repair(uint* triangles, int* trirel, edge* hed, int edge_count) {
		int i = blockIdx.x * blockDim.x + threadIdx.x;
		if (i < edge_count) {
			// use volatile variables, this forces register use. Sometimes manual optimization achieves better performance.
			volatile int2 n = hed[i].v;
			volatile int2 a = hed[i].ta;
			volatile int2 b = hed[i].tb;
			volatile int2 op = hed[i].op;
			// if the t_a pair of indexes are broken
			if ((n.x != triangles[a.x] || n.y != triangles[a.y])) {
				// then repair them.
				LOG("[mcleap_kernel_repair] Repair edge %d, vertices %d %d\n", i, n.x, n.y);

				int t_index = trirel[a.x / 3];
				if (triangles[3 * t_index + 0] == n.x) {
					a.x = 3 * t_index + 0;
					triangles[3 * t_index + 1] == n.y ? (a.y = 3 * t_index + 1, op.x = 3 * t_index + 2) : (a.y = 3 * t_index + 2, op.x = 3 * t_index + 1);
				}
				else if (triangles[3 * t_index + 1] == n.x) {
					a.x = 3 * t_index + 1;
					triangles[3 * t_index + 0] == n.y ? (a.y = 3 * t_index + 0, op.x = 3 * t_index + 2) : (a.y = 3 * t_index + 2, op.x = 3 * t_index + 0);
				}
				else if (triangles[3 * t_index + 2] == n.x) {
					a.x = 3 * t_index + 2;
					triangles[3 * t_index + 0] == n.y ? (a.y = 3 * t_index + 0, op.x = 3 * t_index + 1) : (a.y = 3 * t_index + 1, op.x = 3 * t_index + 0);
				}
			}
			if (b.x != -1) {
				if ((n.x != triangles[b.y] || n.y != triangles[b.x])) {
					LOG("[mcleap_kernel_repair] Repair edge %d, vertices %d %d\n", i, n.x, n.y);
					int t_index = trirel[b.x / 3];
					if (triangles[3 * t_index + 0] == n.x) {
						b.y = 3 * t_index + 0;
						triangles[3 * t_index + 1] == n.y ? (b.x = 3 * t_index + 1, op.y = 3 * t_index + 2) : (b.x = 3 * t_index + 2, op.y = 3 * t_index + 1);
					}
					else if (triangles[3 * t_index + 1] == n.x) {
						b.y = 3 * t_index + 1;
						triangles[3 * t_index + 0] == n.y ? (b.x = 3 * t_index + 0, op.y = 3 * t_index + 2) : (b.x = 3 * t_index + 2, op.y = 3 * t_index + 0);
					}
					else if (triangles[3 * t_index + 2] == n.x) {
						b.y = 3 * t_index + 2;
						triangles[3 * t_index + 0] == n.y ? (b.x = 3 * t_index + 0, op.y = 3 * t_index + 1) : (b.x = 3 * t_index + 1, op.y = 3 * t_index + 0);
					}
				}
			}
			hed[i].ta = make_int2(a.x, a.y);
			hed[i].tb = make_int2(b.x, b.y);
			hed[i].op = make_int2(op.x, op.y);
		}
	}

	template<unsigned int block_size>
	__global__ void correctTrianglesKernel(MCLEAP_VEC* mesh_data, uint* triangles, edge* hed, int edge_count, int* listo, int* trirel, int* trireservs, int* v_to_he) {
		const int i = blockIdx.x * blockDim.x + threadIdx.x; //! + 2 flop
		__shared__ int2 a_shared_array[block_size];
		__shared__ int2 b_shared_array[block_size];
		__shared__ int2 op_shared_array[block_size];
		if (i < edge_count) {
			a_shared_array[threadIdx.x] = hed[i].ta;
			b_shared_array[threadIdx.x] = hed[i].tb;
			op_shared_array[threadIdx.x] = hed[i].op;

			if (b_shared_array[threadIdx.x].x != -1) {
				int test = invertedTriangleTest(mesh_data[triangles[op_shared_array[threadIdx.x].x]], mesh_data[triangles[op_shared_array[threadIdx.x].y]], mesh_data[triangles[a_shared_array[threadIdx.x].x]], mesh_data[triangles[a_shared_array[threadIdx.x].y]]);
				if (test == -1)listo[1] = 1;
				if (test == -2)listo[1] = (listo[1] == 1 ? 1 : 2); 
				if (test > 0) {
					listo[0] = 0;
					// exclusion part
					if (atomicExch(&(trireservs[a_shared_array[threadIdx.x].y / 3]), i) == -1 && atomicExch(&(trireservs[b_shared_array[threadIdx.x].y / 3]), i) == -1) { //!  + 8 flop		
						LOG("[correctTrianglesKernel] Flip edge %d, triangles %d %d, t1 "
							"vertices %d %d %d, t2 vertices %d %d %d\n",
							i, a_shared_array[threadIdx.x].y / 3,
							b_shared_array[threadIdx.x].y / 3,
							triangles[a_shared_array[threadIdx.x].x],
							triangles[a_shared_array[threadIdx.x].y],
							triangles[op_shared_array[threadIdx.x].x],
							triangles[b_shared_array[threadIdx.x].x],
							triangles[b_shared_array[threadIdx.x].y],
							triangles[op_shared_array[threadIdx.x].y]);
						flip_edge(triangles,
							hed,
							trirel,
							trireservs,
							v_to_he,
							a_shared_array,
							b_shared_array,
							op_shared_array);
					}
				}
			}
		}
	}

	__global__ void move_vertices_kernel(MCLEAP_VEC* mesh_data, int vertex_count, MCLEAP_REAL* displacement, MCLEAP_REAL multiplier) {
		const int i = blockIdx.x * blockDim.x + threadIdx.x; //! + 2 flop
		if (i < vertex_count) {
			mesh_data[i].x += multiplier * displacement[i*2+0];
			mesh_data[i].y += multiplier * displacement[i*2+1];
		}
	}

};