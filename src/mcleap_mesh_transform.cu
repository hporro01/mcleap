#include "../include/mcleap_mesh.cuh"
#include "../lib/predicates.h"

#include <fstream>
#include <map>
#include <utility>

#include <iostream>

#include "kernels.cuh"
#include "semi_gpu_kernels.cuh"

#include "helper.cuh"

namespace MCleap {

	constexpr int default_block_size = 256;

	RESULT_CODE delaunay_transformation(mcleap_mesh* m) {
		int h_listo[1], it = 0;

		map_cuda_buffers(m);

		int block_size = default_block_size;
		dim3 dimBlock(block_size);
		dim3 dimGrid((m->num_edges + block_size - 1) / dimBlock.x);
		dim3 dimBlockInit(block_size);
		dim3 dimGridInit((m->num_triangles + block_size - 1) / dimBlock.x);

		//cudaHostAlloc((void**)&h_listo, sizeof(int), cudaHostAllocMapped);
		h_listo[0] = 0;
		//cudaHostGetDevicePointer((void**)&m->d_mesh->d_listo, (void*)h_listo, 0);
		while (!h_listo[0]) {
			h_listo[0] = 1;
			cudaMemcpy(m->d_mesh->d_listo, h_listo, sizeof(int), cudaMemcpyHostToDevice);
			cudaDeviceSynchronize();

			init_device_dual_arrays_int << <dimGridInit, dimBlockInit >> > (m->d_mesh->trirel, m->d_mesh->trireservs, m->num_triangles, -1);  // Takes around 10^-5 secs
			cudaDeviceSynchronize();
			delaunay_kernel<256> << <dimGrid, dimBlock >> > (m->d_vbo_v, m->d_t, m->d_mesh->he, m->num_edges, m->d_mesh->d_listo, m->d_mesh->trirel, m->d_mesh->trireservs, m->d_mesh->v_to_he);

			cudaDeviceSynchronize();
			cudaMemcpy(h_listo, m->d_mesh->d_listo, sizeof(int), cudaMemcpyDeviceToHost);
			if (h_listo[0]) {
				break;
			}
			mcleap_kernel_repair << <dimGrid, dimBlock >> > (m->d_t, m->d_mesh->trirel, m->d_mesh->he, m->num_edges);  // update
			it++;
		}

		//cudaFreeHost(h_listo);

		return SUCCESS;
	}

	RESULT_CODE fix_inverted_triangles(mcleap_mesh* m) {
		int h_listo[2], it = 0;

		map_cuda_buffers(m);

		int block_size = default_block_size;
		dim3 dimBlock(block_size);
		dim3 dimGrid((m->num_edges + block_size - 1) / dimBlock.x);
		dim3 dimBlockInit(block_size);
		dim3 dimGridInit((m->num_triangles + block_size - 1) / dimBlock.x);

		//cudaHostAlloc((void**)&h_listo, sizeof(int), cudaHostAllocMapped);
		h_listo[0] = 0;
		//int count = 0;
		//cudaHostGetDevicePointer((void**)&m->d_mesh->d_listo, (void*)h_listo, 0);
		while (!h_listo[0]) {
			h_listo[0] = 1;
			h_listo[1] = 0;
			cudaMemcpy(m->d_mesh->d_listo, h_listo, 2 * sizeof(int), cudaMemcpyHostToDevice);
			cudaDeviceSynchronize();
			init_device_dual_arrays_int << <dimGridInit, dimBlockInit >> > (m->d_mesh->trirel, m->d_mesh->trireservs, m->num_triangles, -1);  // Takes around 10^-5 secs
			cudaDeviceSynchronize();
			correctTrianglesKernel<256> << <dimGrid, dimBlock >> > (m->d_vbo_v, m->d_t, m->d_mesh->he, m->num_edges, m->d_mesh->d_listo, m->d_mesh->trirel, m->d_mesh->trireservs, m->d_mesh->v_to_he);

			cudaDeviceSynchronize();
			cudaMemcpy(h_listo, m->d_mesh->d_listo, 2 * sizeof(int), cudaMemcpyDeviceToHost);
			if (h_listo[1]==1) {
				LOG("[fix_inverted_triangles] Bicell with 0 area found\n");
				return ZERO_AREA_FOUND;
			}
			if (h_listo[1] == 2) {
				return SHOULD_DECREASE_STEP;
			}
			if (h_listo[0]) {
				break;
			}
			mcleap_kernel_repair << <dimGrid, dimBlock >> > (m->d_t, m->d_mesh->trirel, m->d_mesh->he, m->num_edges);  // update
			it++;
		}

		//int count = 0;
		//cudaHostGetDevicePointer((void**)&m->d_mesh->d_listo, (void*)h_listo, 0);		

		//cudaFreeHost(h_listo);

		return SUCCESS;
	}


	RESULT_CODE semi_gpu_delaunay_transformation(mcleap_mesh* m) {
		int h_listo[1], it = 0;
		int* h_predicates = new int[m->num_edges], * d_predicates;
		for (int i = 0; i < m->num_edges; i++)h_predicates[i] = 0;
		cudaMalloc((void**)&d_predicates, m->num_edges * sizeof(int));

		// Map resources
		map_cuda_buffers(m);

		int block_size = 256;
		dim3 dimBlock(block_size);
		dim3 dimGrid((m->num_edges + block_size - 1) / dimBlock.x);
		dim3 dimBlockInit(block_size);
		dim3 dimGridInit((m->num_triangles + block_size - 1) / dimBlock.x);

		// cudaHostAlloc((void**)&h_listo, sizeof(int), cudaHostAllocMapped);
		h_listo[0] = 0;
		// cudaHostGetDevicePointer((void**)&m->d_mesh->d_listo, (void*)h_listo, 0);
		while (!h_listo[0]) {
			h_listo[0] = 1;
			cudaMemcpy(m->d_mesh->d_listo, h_listo, sizeof(int), cudaMemcpyHostToDevice);
			cudaDeviceSynchronize();
			init_device_dual_arrays_int << <dimGridInit, dimBlockInit >> > (m->d_mesh->trirel, m->d_mesh->trireservs, m->num_triangles, -1);  // Takes around 10^-5 secs
			cudaDeviceSynchronize();

			cudaMemcpy(m->v, m->d_vbo_v, m->num_vertices * sizeof(MCLEAP_REAL) * 2, cudaMemcpyDeviceToHost);
			cudaMemcpy(m->he, m->d_mesh->he, m->num_edges * sizeof(edge), cudaMemcpyDeviceToHost);
			cudaMemcpy(m->t, m->d_t, m->num_triangles * sizeof(uint) * 3, cudaMemcpyDeviceToHost);
			for (int i = 0; i < m->num_edges; i++) {
				if (m->he[i].op.y == -1) {
					h_predicates[i] = 0;
					continue;
				}
				double a[2] = { (double)m->v[m->t[m->he[i].ta.y] * 2 + 0],
								(double)m->v[m->t[m->he[i].ta.y] * 2 + 1] };
				double b[2] = { (double)m->v[m->t[m->he[i].op.x] * 2 + 0],
								(double)m->v[m->t[m->he[i].op.x] * 2 + 1] };
				double c[2] = { (double)m->v[m->t[m->he[i].ta.x] * 2 + 0],
								(double)m->v[m->t[m->he[i].ta.x] * 2 + 1] };
				double d[2] = { (double)m->v[m->t[m->he[i].op.y] * 2 + 0],
								(double)m->v[m->t[m->he[i].op.y] * 2 + 1] };
				if (!(orient2d(a, b, c) > 0))
					LOG("Triangle %d is non ccw, vertices %d %d %d\n", m->he[i].ta.y / 3,
						m->t[m->he[i].ta.y], m->t[m->he[i].op.x], m->t[m->he[i].ta.x]);
				h_predicates[i] =
					((orient2d(a, b, c) > 0 && orient2d(b, c, d) > 0 &&
						orient2d(c, d, a) > 0 && orient2d(d, a, b) > 0) ||
						(orient2d(a, b, c) < 0 && orient2d(b, c, d) < 0 &&
							orient2d(c, d, a) < 0 && orient2d(d, a, b) < 0)) &&
					((incircle(a, b, c, d) > 0) ||
						(incircle(c, d, a, b) > 0));
			}

			cudaDeviceSynchronize();
			cudaMemcpy(d_predicates, h_predicates, m->num_edges * sizeof(int), cudaMemcpyHostToDevice);

			cudaDeviceSynchronize();
			semi_gpu_delaunay_kernel<256> << <dimGrid, dimBlock >> > (m->d_vbo_v, m->d_t, m->d_mesh->he, m->num_edges, m->d_mesh->d_listo, m->d_mesh->trirel, m->d_mesh->trireservs, d_predicates, m->d_mesh->v_to_he);

			cudaDeviceSynchronize();
			cudaMemcpy(h_listo, m->d_mesh->d_listo, sizeof(int), cudaMemcpyDeviceToHost);
			if (h_listo[0]) {
				break;
			}
			mcleap_kernel_repair << <dimGrid, dimBlock >> > (m->d_t, m->d_mesh->trirel, m->d_mesh->he, m->num_edges);  // update
			it++;
		}

		// cudaFreeHost(h_listo);
		cudaFree(d_predicates);
		delete h_predicates;

		return SUCCESS;
	}



	RESULT_CODE semi_gpu_fix_inverted_triangles(mcleap_mesh* m) {
		int h_listo[2], it = 0;
		int* h_predicates = new int[m->num_edges];
		int* d_predicates;
		for (int i = 0; i < m->num_edges; i++)h_predicates[i] = 0;
		cudaMalloc((void**)&d_predicates, m->num_edges * sizeof(int));
		// Map resources
		map_cuda_buffers(m);

		int block_size = 256;
		dim3 dimBlock(block_size);
		dim3 dimGrid((m->num_edges + block_size - 1) / dimBlock.x);
		dim3 dimBlockInit(block_size);
		dim3 dimGridInit((m->num_triangles + block_size - 1) / dimBlock.x);

		// cudaHostAlloc((void**)&h_listo, sizeof(int), cudaHostAllocMapped);
		h_listo[0] = 0;
		h_listo[1] = 1;
		while (!h_listo[0]) {

			h_listo[0] = 1;
			h_listo[1] = 0;
			cudaMemcpy(m->d_mesh->d_listo, h_listo, 2 * sizeof(int), cudaMemcpyHostToDevice);
			cudaDeviceSynchronize();
			init_device_dual_arrays_int << <dimGridInit, dimBlockInit >> > (m->d_mesh->trirel, m->d_mesh->trireservs, m->num_triangles, -1);  // Takes around 10^-5 secs
			cudaDeviceSynchronize();

			cudaMemcpy(m->v, m->d_vbo_v, m->num_vertices * sizeof(MCLEAP_REAL) * 2, cudaMemcpyDeviceToHost);
			cudaMemcpy(m->he, m->d_mesh->he, m->num_edges * sizeof(edge), cudaMemcpyDeviceToHost);
			cudaMemcpy(m->t, m->d_t, m->num_triangles * sizeof(uint) * 3, cudaMemcpyDeviceToHost);

			for (int i = 0; i < m->num_edges; i++) {
				if (m->he[i].op.y == -1) {
					h_predicates[i] = 0;
					continue;
				}

				double a0[2] = { (double)m->v[m->t[m->he[i].ta.y] * 2 + 0],
								(double)m->v[m->t[m->he[i].ta.y] * 2 + 1] };
				double b0[2] = { (double)m->v[m->t[m->he[i].op.x] * 2 + 0],
								(double)m->v[m->t[m->he[i].op.x] * 2 + 1] };
				double c0[2] = { (double)m->v[m->t[m->he[i].ta.x] * 2 + 0],
								(double)m->v[m->t[m->he[i].ta.x] * 2 + 1] };
				double d0[2] = { (double)m->v[m->t[m->he[i].op.y] * 2 + 0],
								(double)m->v[m->t[m->he[i].op.y] * 2 + 1] };
				double d = orient2d(c0, d0, a0);
				double s = orient2d(c0, b0, a0);
				double t = orient2d(c0, d0, b0);
				h_predicates[i] = ((d < 0 && s <= 0 && t <= 0) ||
					(d > 0 && s >= 0 && t >= 0) ||
					(s < 0 && d <= 0 && -t <= 0) ||
					(s > 0 && d >= 0 && -t >= 0)) > 0;
			}
			cudaMemcpy(d_predicates, h_predicates, m->num_edges * sizeof(int), cudaMemcpyHostToDevice);

			cudaDeviceSynchronize();

			semi_gpu_correctTrianglesKernel<256> << <dimGrid, dimBlock >> > (m->d_vbo_v, m->d_t, m->d_mesh->he, m->num_edges, m->d_mesh->d_listo, m->d_mesh->trirel, m->d_mesh->trireservs, d_predicates, m->d_mesh->v_to_he);

			cudaDeviceSynchronize();
			cudaMemcpy(h_listo, m->d_mesh->d_listo, 2 * sizeof(int), cudaMemcpyDeviceToHost);

			if (h_listo[1]) {
				LOG("[fix_inverted_triangles] Bicell with 0 area found\n");
				std::string filename("error.off");
				save_off_mesh(m, filename);
				return FAILURE;
			}
			if (h_listo[0]) {
				break;
			}
			mcleap_kernel_repair << <dimGrid, dimBlock >> > (m->d_t, m->d_mesh->trirel, m->d_mesh->he, m->num_edges);  // update
			it++;
		}

		// cudaFreeHost(h_listo);
		cudaFree(d_predicates);
		delete h_predicates;
		return SUCCESS;
	}

	double move_vertices(mcleap_mesh* m_original, mcleap_mesh* m_copy, MCLEAP_REAL* displacement, MCLEAP_REAL to_move) {
		deep_device_mesh_copy(m_copy, m_original);

		map_cuda_buffers(m_copy);

		int block_size = 256;
		dim3 dimBlock(block_size);
		dim3 dimGrid((m_original->num_vertices + block_size - 1) / dimBlock.x);
		dim3 dimBlockInit(block_size);
		dim3 dimGridInit((m_original->num_vertices + block_size - 1) / dimBlock.x);

		move_vertices_kernel << <dimGrid, dimBlock >> > (m_copy->d_vbo_v, m_original->num_vertices, displacement, to_move);

		MCLEAP_REAL moved = -1.0;
		MCLEAP_REAL lilmove = to_move;
		while (fix_inverted_triangles(m_copy) == SHOULD_DECREASE_STEP) {
			lilmove *= 0.5;
			moved = move_vertices(m_original, m_copy, displacement, lilmove);
		}
		if (moved < 0.0) moved = to_move;

		LOG("Depth of the search: %d\n", floorf(log2(lilmove)));

		delaunay_transformation(m_copy);

		deep_device_mesh_copy(m_original, m_copy);

		return moved;
	}


	RESULT_CODE move_vertices(mcleap_mesh* m_original, mcleap_mesh* m_copy, MCLEAP_REAL* displacement) {
		MCLEAP_REAL to_move_left = 1.0;
		int count = 0;
		while (to_move_left > 0.0) {
			count++;
			MCLEAP_REAL disp = move_vertices(m_original, m_copy, displacement, to_move_left);
			to_move_left -= disp;
			//printf("disp: %lf\n",disp);
		}
		LOG("Number of substeps in order to move the points: %d\n", count);
		return SUCCESS;
	}

	RESULT_CODE move_vertices(mcleap_mesh* m, MCLEAP_REAL* displacement) {
		mcleap_mesh* m_copy = init_empty_mesh(m->num_vertices, m->num_edges, m->num_triangles);
		deep_device_mesh_copy(m_copy, m);

		move_vertices(m, m_copy, displacement);
		delete m_copy;
		return SUCCESS;
	}
};