#pragma once

#include "kernels.cuh"

namespace MCleap {

	template<unsigned int block_size>
	__global__ void semi_gpu_delaunay_kernel(MCLEAP_VEC* mesh_data, uint* triangles, edge* hed, int edge_count, int* listo, int* trirel, int* trireservs, int* predicates, int* v_to_he) {

		const int i = blockIdx.x * blockDim.x + threadIdx.x; //! + 2 flop
		__shared__ int2 a_shared_array[block_size];
		__shared__ int2 b_shared_array[block_size];
		__shared__ int2 op_shared_array[block_size];
		if (i < edge_count) {
			a_shared_array[threadIdx.x] = hed[i].ta;
			b_shared_array[threadIdx.x] = hed[i].tb;
			op_shared_array[threadIdx.x] = hed[i].op;

			if (b_shared_array[threadIdx.x].x != -1) {
				if (predicates[i]) {
					listo[0] = 0;
					// exclusion part
					LOG("[semi_gpu_delaunay_kernel] FLIP edge %d\n", i);
					if (atomicExch(&(trireservs[a_shared_array[threadIdx.x].y / 3]), i) == -1 && atomicExch(&(trireservs[b_shared_array[threadIdx.x].y / 3]), i) == -1) { //!  + 8 flop
						flip_edge(triangles,
							hed,
							trirel,
							trireservs,
							v_to_he,
							a_shared_array,
							b_shared_array,
							op_shared_array);
					}
				}
			}
		}
	}

	template<unsigned int block_size>
	__global__ void semi_gpu_correctTrianglesKernel(MCLEAP_VEC* mesh_data, uint* triangles, edge* hed, int edge_count, int* listo, int* trirel, int* trireservs, int* predicates, int* v_to_he) {
		const int i = blockIdx.x * blockDim.x + threadIdx.x; //! + 2 flop
		__shared__ int2 a_shared_array[block_size];
		__shared__ int2 b_shared_array[block_size];
		__shared__ int2 op_shared_array[block_size];
		if (i < edge_count) {
			a_shared_array[threadIdx.x] = hed[i].ta;
			b_shared_array[threadIdx.x] = hed[i].tb;
			op_shared_array[threadIdx.x] = hed[i].op;

			if (b_shared_array[threadIdx.x].x != -1) {
				if(predicates[i]) {
					listo[0] = 0;
					// exclusion part
					if (atomicExch(&(trireservs[a_shared_array[threadIdx.x].y / 3]), i) == -1 && atomicExch(&(trireservs[b_shared_array[threadIdx.x].y / 3]), i) == -1) { //!  + 8 flop
						LOG("[semi_gpu_correctTrianglesKernel] FLIP edge %d\n", i);
						flip_edge(triangles,
							hed,
							trirel,
							trireservs,
							v_to_he,
							a_shared_array,
							b_shared_array,
							op_shared_array);
					}
				}
			}
		}
	}

};