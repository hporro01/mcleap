#pragma once

#include "mcleap_mesh.cuh"

using MCleap::MCLEAP_VEC;
using MCleap::MCLEAP_REAL;

inline __device__ __host__ MCLEAP_VEC distVec(MCLEAP_VEC a, MCLEAP_VEC b) {
	return b - a;
}

__device__ MCLEAP_REAL mcleap_d_magnitude(MCLEAP_VEC v) {
	return __fsqrt_rn(powf(v.x, 2) + powf(v.y, 2));
}

inline __device__ MCLEAP_REAL dist(MCLEAP_VEC v0, MCLEAP_VEC v1) {
	return mcleap_d_magnitude(v0 - v1);
}

#define PEEK_NUM -1

// half edge operations
#define TWIN_EDGE_INDEX(edge_index) (edge_index^1)
#define NEXT_EDGE_INDEX(edge,edge_index) ((edge_index % 2) ? edge.e.y : edge.e.x)

template<typename Functor, int queueSize, int visitedSizePerVertex>
__global__ void calcFRNN_dfs(Functor f, MCLEAP_VEC* mesh_data, MCleap::uint* triangles, MCleap::edge* hed, int* v_to_he, int* visited, int vertex_count, const float rad) {

	const int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < vertex_count) {
		MCLEAP_VEC i_pos = mesh_data[i];
		int visitCounter = 0;
		int queue[queueSize];
		queue[0] = i;
		int queueCounter = 1;
		while (queueCounter > 0) {
			int visiting = queue[--queueCounter]; // vertex we are visiting now

			bool alreadyVisited = false;
			for (int j = 0; j < visitCounter; j++) {
				if (visited[i * visitedSizePerVertex + j] == visiting) {
					alreadyVisited = true;
				}
			}
			if (alreadyVisited)continue; // If we already visited this vertex, we just continue with the next one

			if (visitCounter < visitedSizePerVertex)
				visited[i * visitedSizePerVertex + visitCounter++] = visiting; // Make sure we dont visit again this vertex
			else return; // VERY DANGEROUS. It means that we filled our estimated amount of visited vertices per vertex

			int edge_index = v_to_he[visiting];
			MCleap::edge actual = hed[edge_index / 2];
			int first_vertex = visiting, actual_vertex = visiting;

			edge_index = NEXT_EDGE_INDEX(actual, edge_index); // next edge index
			actual = hed[edge_index / 2]; // next edge
			actual_vertex = (edge_index % 2) ? actual.v.y : actual.v.x; // next vertex

			first_vertex = actual_vertex;

			do {
				// Now check if it has to be checked
				alreadyVisited = false;
				for (int j = 0; j < visitCounter; j++) {
					if (visited[i * visitedSizePerVertex + j] == actual_vertex) {
						alreadyVisited = true;
					}
				}
				bool alreadyOnQueue = false;
				for (int j = 0; j < queueCounter; j++) {
					if (queue[j] == actual_vertex)
						alreadyOnQueue = true;
				}
				MCLEAP_VEC dist_vec = distVec(mesh_data[actual_vertex], i_pos);
				if (!(alreadyVisited || alreadyOnQueue)) {
					double dist = mcleap_d_magnitude(dist_vec);
					if (dist <= rad) {

						// apply the force
						// we assume that the functor only needs the indices to the buffer
						f(i, actual_vertex, dist_vec, dist);
						if (queueCounter < queueSize) // If there is no space in the queue is also a disaster
							queue[queueCounter++] = actual_vertex;
					}
					else {
						if (visitCounter < visitedSizePerVertex)
							visited[i * visitedSizePerVertex + visitCounter++] = actual_vertex;
						else return;
					}
				};

				edge_index = NEXT_EDGE_INDEX(actual, edge_index); // next edge index
				edge_index = TWIN_EDGE_INDEX(edge_index); // next twin edge index (-1 if its the second one, +1 if its the first one)
				actual = hed[edge_index / 2]; // next edge
				if ((edge_index % 2 == 1) && (actual.tb.x == -1))break; // border case

				edge_index = NEXT_EDGE_INDEX(actual, edge_index); // next edge index
				actual = hed[edge_index / 2]; // next edge
				actual_vertex = (edge_index % 2) ? actual.v.y : actual.v.x; // next vertex

			} while (actual_vertex != first_vertex);
		};
		//if(visitCounter>10)printf("i: %d visit counter: %d\n", i, visitCounter);
	}
}

template<typename Functor, int latestFrontierSize, int newFrontierSize>
__global__ void calcFRNN_frontier(Functor f, MCLEAP_VEC* mesh_data, MCleap::uint* triangles, MCleap::edge* hed, int* v_to_he, int* frontier, int vertex_count, const float rad) {
	const int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < vertex_count && i>3) {
		MCLEAP_VEC i_pos = mesh_data[i];

		// there are 3 frontiers that have to be taken into account
		// the last one is the inner one, closer to the vertex
		// the current one is the one we are using now to calculate a new one
		// the new one is the one we are currently computing

		frontier[newFrontierSize * i + 0] = i;
		int frontierCounter = 1; // list of vertices located in the frontier
		int lastFrontierLeft = 0;
		int lastFrontierRight = 1; // range of the last frontier

		int currFrontierLeft = 1;
		int currFrontierRight = 1;

		int edge_index = v_to_he[i];
		int first_he = edge_index;

		// populating currFrontier
		do {
			MCleap::edge actual1 = hed[edge_index / 2]; // next edge
			edge_index = NEXT_EDGE_INDEX(actual1, edge_index); // next edge index
			MCleap::edge actual2 = hed[edge_index / 2]; // next next edge
			edge_index = NEXT_EDGE_INDEX(actual2, edge_index); // next next edge index
			edge_index = TWIN_EDGE_INDEX(edge_index); // next twin edge index (-1 if its the second one, +1 if its the first one)
			if ((edge_index % 2 == 1) && (actual2.tb.x == -1))break; // border case (only happens at half edges at the 4 borders)
			MCleap::edge actual3 = hed[edge_index / 2]; // next edge
			int actual_vertex = (actual3.v.y == i ? actual3.v.x : actual3.v.y); // next vertex
			frontier[i * newFrontierSize + currFrontierRight] = actual_vertex; // we add this vertex to the current frontier
			currFrontierRight++;
		} while (edge_index != first_he);

		int nextFrontierLeft = currFrontierRight;
		int nextFrontierRight = nextFrontierLeft;

		while (currFrontierLeft < currFrontierRight) {
			for (int p = currFrontierLeft; p < currFrontierRight; p++) {
				int v_index = frontier[i * newFrontierSize + p];
				MCLEAP_VEC dist_vec = distVec(mesh_data[v_index], i_pos);
				double dist = mcleap_d_magnitude(dist_vec);

				if (dist > rad)continue;
				f(i, v_index, dist_vec, dist);

				edge_index = v_to_he[v_index];
				first_he = edge_index;
				do {
					MCleap::edge actual1 = hed[edge_index / 2]; // next edge
					edge_index = NEXT_EDGE_INDEX(actual1, edge_index); // next edge index
					MCleap::edge actual2 = hed[edge_index / 2]; // next next edge
					edge_index = NEXT_EDGE_INDEX(actual2, edge_index); // next next edge index
					edge_index = TWIN_EDGE_INDEX(edge_index); // next twin edge index (-1 if its the second one, +1 if its the first one)
					if ((edge_index % 2 == 1) && (actual2.tb.x == -1))break; // border case (only happens at half edges at the 4 borders)
					MCleap::edge actual3 = hed[edge_index / 2]; // next edge
					int actual_vertex = (actual3.v.y == i ? actual3.v.x : actual3.v.y); // next vertex
					
					// check if we already have this vertex in the frontier array
					bool is_in_frontier = false;

					// THE STARTING VALUE OF F IS REALLY IMPORTANT. This doesnt work if the neighbourhood is bigger than newFrontierSize
					for (int f = 0; f < nextFrontierRight; f++) {
						int vf_index = frontier[i * newFrontierSize + f];
						if (vf_index == actual_vertex) { is_in_frontier = true; break; }
					}
					if (!is_in_frontier) {
						frontier[i * newFrontierSize + nextFrontierRight] = actual_vertex; // we add this vertex to the next frontier
						nextFrontierRight++;
					}
				} while (edge_index != first_he);

			}
			// update frontier counters
			lastFrontierLeft = currFrontierLeft;
			lastFrontierRight = currFrontierRight;
			currFrontierLeft = nextFrontierLeft;
			currFrontierRight = nextFrontierRight;
			nextFrontierLeft = nextFrontierRight;

			//if(currFrontierRight>10)printf("i: %d currFrontRight: %d\n", i, currFrontierRight);

		}
	}
}