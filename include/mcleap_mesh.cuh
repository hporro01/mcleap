#pragma once

#include "MCleap.h"

#include <cuda.h>

#ifdef BUILD_MCLEAP_WITH_OPENGL
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <cuda_gl_interop.h>
#endif


namespace MCleap {

	template<class T>
	struct vec2 {
		T x, y;
		__host__ __device__ inline T& operator[](const int& i) {return ((T*)this)[i];}
		__host__ __device__ vec2<T>& operator+=(const vec2<T>& v) {
			x += v.x;
			y += v.y;
			return *this;
		}
		__host__ __device__ vec2<T>& operator-=(const vec2<T>& v) {
			x -= v.x;
			y -= v.y;
			return *this;
		}
	};

	template<class T>
	__host__ __device__ inline vec2<T> operator+(const vec2<T>& v, const vec2<T>& q) {
		return vec2<T>{v.x + q.x, v.y + q.y};
	}
	template<class T>
	__host__ __device__ inline vec2<T> operator-(const vec2<T>& v, const vec2<T>& q) {
		return vec2<T>{v.x - q.x, v.y - q.y};
	}
	template<class T>
	__host__ __device__ inline vec2<T> operator*(const vec2<T>& v, const T& a) {
		return vec2<T>{v.x * a, v.y * a};
	}
	template<class T>
	__host__ __device__ inline vec2<T> operator*(const T& a, const vec2<T>& v) {
		return vec2<T>{v.x * a, v.y * a};
	}
	template<class T>
	__host__ __device__ inline vec2<T> operator/(const vec2<T>& v, const T& a) {
		return vec2<T>{v.x / a, v.y / a};
	}


#ifdef MCLEAP_DOUBLE_PRECISION
	using veclf2 = vec2<double>;
	using MCLEAP_VEC = veclf2;
#else
	using vecf2 = vec2<float>;
	using MCLEAP_VEC = vecf2;
#endif

	static bool initialized_cuda = false;

	// This represents 2 half edges
	struct edge {
		int2 ta = { -1, -1 };
		int2 tb = { -1, -1 };
		int2 v = { -1, -1 };
		int2 op = { -1, -1 };
		int2 e = { -1, -1 };
	};

	// GPU mesh data
	struct mcleap_device_mesh {

#ifdef BUILD_MCLEAP_WITH_OPENGL
		struct cudaGraphicsResource* vbo_v_cuda, * ebo_t_cuda;
		GLuint vbo_v, ebo_t, vao;
#else
		uint* vbo_v, *ebo_t;
#endif
		int* v_to_he;
		int* t_to_he;
		int* trirel, * trireservs;
		int* d_listo;
		edge* he;

		~mcleap_device_mesh() {
			cudaFree(v_to_he);
			cudaFree(t_to_he);
			cudaFree(he);
			cudaFree(d_listo);
			cudaFree(trirel);
			cudaFree(trireservs);
#ifdef BUILD_MCLEAP_WITH_OPENGL
			cudaGraphicsUnregisterResource(vbo_v_cuda);
			cudaGraphicsUnregisterResource(ebo_t_cuda);
			glDeleteBuffers(1, &vbo_v);
			glDeleteBuffers(1, &ebo_t);
			glDeleteVertexArrays(1, &vao);
#else
			cudaFree(vbo_v);
			cudaFree(ebo_t);
#endif
		}
	};

	// mcleap_mesh always owns an mcleap_device_mesh
	struct mcleap_mesh {
		uint num_vertices = 0;
		uint num_triangles = 0;
		uint num_edges = 0;
		MCLEAP_REAL* v;
		uint* t;
		uint* v_to_he;
		uint* t_to_he;
		edge* he;

		mcleap_device_mesh* d_mesh = nullptr;

		~mcleap_mesh() {
			delete[] v;
			delete[] v_to_he;
			delete[] t;
			delete[] t_to_he;
			delete[] he;
			unmap_cuda_buffers(this);
			delete d_mesh;
		}

		MCLEAP_VEC* d_vbo_v;
		uint* d_t;
		size_t bytes = 0;

		/** @brief True if  we already mapped this class GPU cuda buffers, and false otherwise */
		bool h_buffers_cuda_mapped = false;

	};

	mcleap_mesh* load_triangle_soup(std::string& filename);
	void build_from_soup(mcleap_mesh* m);
	void transfer_to_device(mcleap_mesh* m);
	void transfer_to_host(mcleap_mesh* m);

};  // namespace MCleap