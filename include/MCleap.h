#pragma once

#ifdef BUILD_MCLEAP_WITH_OPENGL
#include <GL/glew.h>
#endif
#include <string>

/// Public interface of the library
namespace MCleap {

#ifdef MCLEAP_DOUBLE_PRECISION
	using MCLEAP_REAL = double;
#else
	using MCLEAP_REAL = float;
#endif

	// -------------- Types & variables defined by the library ----------------- //
	enum RESULT_CODE {
		SHOULD_DECREASE_STEP = -3,
		ZERO_AREA_FOUND = -2,
		ASSERTION_FAILURE = -1,
		FAILURE = 0,
		SUCCESS = 1,
	};

	using uint = unsigned int;

	struct mcleap_mesh;

	/// ------- Getters and Setters ------------ ///
#ifdef BUILD_MCLEAP_WITH_OPENGL
	/**
	 * Returns the OpenGL vbo number of the vertices buffer of a given mesh.
	 * It should be already initialized.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @return The vbo with the positions of the mesh vertices.
	 */
	GLuint get_vertex_vbo(mcleap_mesh* m);
	/**
	 * Returns the OpenGL ebo number of triangle index buffer of a given mesh.
	 * It should be already initialized.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @return The ebo with the triangle index buffer.
	 */
	GLuint get_triangles_ebo(mcleap_mesh* m);
	/**
	 * Returns the OpenGL vbo number of the vertices buffer of a given mesh.
	 * It should be already initialized.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @return The OpenGL vao to render the mesh.
	 */
	GLuint get_vao(mcleap_mesh* m);
	/**
	 * Returns the triangle index buffer on host memory (stored on host memory).
	 *
	 * @param m Pointer to an mcleap_mesh object. It should be already initialized.
	 * @return The the triangle index buffer on host memory.
	 */
	GLuint* get_host_indices(mcleap_mesh* m);
#endif

	// >>>> Operations >>>>> // 

	// -------------------- I/O ------------------------ //
	/**
	 * Returns a pointer to an initialized mcleap_mesh object.
	 * It reads the triangulation specified in the file called filename, loading the
	 * triangulation to host memory, builds the necessary pointers and then transfers
	 * the structure to device memory.
	 *
	 * @param filename Name of the file that has to be read.
	 * @return Returns An mcleap_mesh pointer.
	 */
	mcleap_mesh* load_off_mesh(std::string& filename);
	/**
	 * Saves the device mesh in off file format, in a file called filename.
	 * TODO: Not implemented yet.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @param filename Name of the file generated.
	 * @return Returns SUCCESS if the saving was successful and FAILURE if not.
	 */
	RESULT_CODE save_off_mesh(mcleap_mesh* m, std::string& filename);
	/**
	 * Prints to standard output the current state of the device buffers.
	 * In order to do so, it transfers device buffers to host memory, overriding old host buffers.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 */
	void print_mesh(mcleap_mesh* m);

	// -------------------- Mesh query ------------------------ //
	/**
	 * Retrieves the number of vertices in the mesh.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @return Number of vertices in the mesh.
	 */
	int vertex_count(mcleap_mesh* m);
	/**
	 * Retrieves the number of edges in the mesh.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @return Number of edges in the mesh.
	 */
	int edge_count(mcleap_mesh* m);
	/**
	 * Retrieves the number of triangles in the mesh.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @return Number of triangles in the mesh.
	 */
	int triangle_count(mcleap_mesh* m);

	/// -------------------- Mesh modification ------------------------ //
	/**
	 * Performs parallel flips on the device mesh, until it is a Delaunay mesh.
	 * It only flips non Delaunay Edges.
	 * See also mcleap_d_delaunay_test_2d in the kernels.cuh file.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @return Returns SUCCESS if the operation was successful and FAILURE if not.
	 */
	RESULT_CODE delaunay_transformation(mcleap_mesh* m);
	/**
	 * Performs parallel flips on the device mesh, until it is a valid mesh.
	 * It only flips inverted triangles, in order to fix them.
	 * See also invertedTriangleTest in the kernels.cuh file.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @return Returns SUCCESS if the operation was successful and FAILURE if not.
	 */
	RESULT_CODE fix_inverted_triangles(mcleap_mesh* m);

	/**
	 * semigpu stuff:
	 * All semigpu stuff does the same as their non semi_gpu counterparts, but
	 * semi_gpu functions compute the geometric predicates in the host memory,
	 * and transfers the results to the device.
	 */

	 /**
	  * Performs parallel flips on the device mesh, until it is a Delaunay mesh.
	  * It only flips non Delaunay Edges.
	  * It computes geometric predicates in host memory using Shewchuk's
	  * implementation of adaptive predicates, and transfers the result to device
	  * memory.
	  *
	  * @param m Pointer to an mcleap_mesh object.
	  * @return Returns SUCCESS if the operation was successful and FAILURE if not.
	  */
	RESULT_CODE semi_gpu_delaunay_transformation(mcleap_mesh* m);
	/**
	 * Performs parallel flips on the device mesh, until it is a valid mesh.
	 * It only flips inverted triangles, in order to fix them.
	 * It computes geometric predicates in host memory using Shewchuk's implementation
	 * of adaptive predicates, and transfers the result to device memory.
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @return Returns SUCCESS if the operation was successful and FAILURE if not.
	 */
	RESULT_CODE semi_gpu_fix_inverted_triangles(mcleap_mesh* m);

	/**
	 * Moves all the vertices on the triangulation according to the displacement buffer.
	 * It assumes that those displacements don't make interpenetration
	 *
	 * @param m Pointer to an mcleap_mesh object.
	 * @param displacement buffer having 2 floats per vertex in the mesh.
	 * @return Returns SUCCESS if the operation was successful and FAILURE if not.
	 */
	RESULT_CODE move_vertices(mcleap_mesh* m, float* displacement);
	/**
	 * Moves all the vertices on the triangulation according to the displacement buffer.
	 * It assumes that those displacements don't make interpenetration
	 *
	 * @param m Pointer to the mcleap_mesh object we want to move.
	 * @param copy Pointer an mcleap_mesh object, which has the same size as m. We will use this as auxiliary data storage if necessary.
	 * @param displacement buffer having 2 floats per vertex in the mesh.
	 * @return Returns SUCCESS if the operation was successful and FAILURE if not.
	 */
	RESULT_CODE move_vertices(mcleap_mesh* m, mcleap_mesh* copy, MCLEAP_REAL* displacement);

	// TODO: Document this
	RESULT_CODE deep_device_mesh_copy(mcleap_mesh* m_dest, mcleap_mesh* m_origin);
	mcleap_mesh* init_empty_mesh(int num_vertices, int num_edges, int num_triangles);
	RESULT_CODE map_cuda_buffers(mcleap_mesh* m);
	RESULT_CODE unmap_cuda_buffers(mcleap_mesh* m);

};  // namespace MCleap